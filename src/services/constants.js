// const BASE_ROUTE = "https://pe.doecap.com.br";
// const BASE_ROUTE = "https://al.doecap.com.br";
// const BASE_ROUTE = "https://ba.doecap.com.br";
// const BASE_ROUTE = "https://es.doecap.com.br";
// const BASE_ROUTE = "https://pa.doecap.com.br";
//const BASE_ROUTE = "https://go.doecap.com.br";
// const BASE_ROUTE = "https://am.doecap.com.br";
const BASE_ROUTE = "https://sp.dasorte.com";

// const BASE_ROUTE = "https://doecap.com.br";
//const BASE_ROUTE = "http://mobile-dev.redepos.com.br:8080";

export default Constants = {
  
  // BASE
  
  "APP_VERSION": "1.0.5",
  "APP_BASE": "feira",
  
  // STORAGE
  
  "APP_DATABASE_NAME": "feira",
  "APP_BASE_TITULO": "Feira dá Sorte",
  "APP_DATABASE_NAME_DISPLAY": "Feira da Sorte",
  
  // MAPS
  
  "APP_MAPS": "https://www.pernambucodasorte.com.br/",
  
  // PUSH NOTIFICATION
  
  //	"APP_ID"	 						: "c27bdfcf-caec-456f-bd0c-738b515a888b",//Alagoas
  //	"APP_ID"	 						: "14420452-e4a0-4c7a-aa16-e5da647e0503",//Amazonas
  "APP_ID": "d35d3adc-b4b2-4411-916b-9f6f61aa9ad4",//Bahia
  //	"APP_ID"	 						: "3d685792-1358-421f-8ecb-45a2bc36f6fc",//Capixaba
  //	"APP_ID"	 						: "f4b0fa5c-cf4e-442b-bf45-56a186277420",//Carimbo
  //	"APP_ID"	 						: "b8e7320e-8a57-4289-a83d-39cc3d3310a6",//Goias
  // 	"APP_ID"	 						: "844748f0-10ff-48b8-9094-0a1e1e37089d",//Pernambuco
  //	"APP_ID"	 						: "2a7e883c-6c32-4e54-8ee8-594b87c4a703",//Teste
  
  
  "APP_TIMEOUT": 30000,
  "APP_MSG_CONNECTION": "Sem Conexão!",
  "APP_MSG_TIMEOUT": "Sem resposta do servidor.",
  "APP_MSG_ERROR": "Erro no Servidor, tente em alguns segundos.",
  //"APP_MSG_SEM_EDICAO": "Sem edição disponível no momento.",
  "APP_MSG_SEM_EDICAO": "Estamos adequando nossos serviços para atender você melhor. Voltaremos em alguns minutos.",
  "APP_MSG_UNAVAILABLE": "Servidor Indisponível, tente em alguns minutos.",
  "APP_MSG_NO_RESULT": "Nenhum resultado encontrado.",
  
  //SERVER
  
  //PROD
  // "APP_SERVICE": "https://pe.doecap.com.br/",
  // "URL_FINALIZAR_VENDA": "https://pe.doecap.com.br/vendainternet/mobile/vendamobile/",
  // "URL_JUNO_FINALIZAR_VENDA": "https://al.doecap.com.br/pagamento/?hash=",
  
  // "APP_SERVICE"						: "https://ba.doecap.com.br/",
  // "URL_FINALIZAR_VENDA"				: "https://ba.doecap.com.br/vendainternet/mobile/vendamobile/",
  // "URL_JUNO_FINALIZAR_VENDA"				: "https://ba.doecap.com.br/pagamento/?hash=",
  
  "APP_SERVICE": BASE_ROUTE,
  "URL_FINALIZAR_VENDA": BASE_ROUTE + "/vendainternet/mobile/vendamobile/",
  "URL_AIX_FINALIZAR_VENDA": BASE_ROUTE + "/pagamento/?hash=",
  "FILANTROPIA": "Título(s)",
  "INCENTIVO": "Certificado(s)",
  
  
  // "APP_SERVICE"						: "https://es.doecap.com.br/",
  // "URL_FINALIZAR_VENDA"				: "https://es.doecap.com.br/vendainternet/mobile/vendamobile/",
  // "URL_JUNO_FINALIZAR_VENDA"				: "https://es.doecap.com.br/pagamento/?hash=",
  
  // "APP_SERVICE"						: "https://am.doecap.com.br/",
  // "URL_FINALIZAR_VENDA"				: "https://am.doecap.com.br/vendainternet/mobile/vendamobile/",
  // "URL_JUNO_FINALIZAR_VENDA"				: "https://am.doecap.com.br/pagamento/?hash=",
  
  // "APP_SERVICE"						: "https://pa.doecap.com.br/",
  // "URL_FINALIZAR_VENDA"				: "https://pa.doecap.com.br/vendainternet/mobile/vendamobile/",
  // "URL_JUNO_FINALIZAR_VENDA"				: "https://pa.doecap.com.br/pagamento/?hash=",
  
  // "APP_SERVICE"						: "https://go.doecap.com.br/",
  // "URL_FINALIZAR_VENDA"				: "https://go.doecap.com.br/vendainternet/mobile/vendamobile/",
  // "URL_JUNO_FINALIZAR_VENDA"				: "https://go.doecap.com.br/pagamento/?hash=",
  
  //STAGE
  //"APP_SERVICE"						: "http://mobile-stage.redepos.com.br/",
  //"URL_FINALIZAR_VENDA"				: "http://mobile-stage.redepos.com.br/vendainternet/mobile/vendamobile",
  
  //DEV`
  // "APP_SERVICE": "http://mobile-dev.redepos.com.br:8080/",
  // "URL_FINALIZAR_VENDA": "http://mobile-dev.redepos.com.br:8080/vendainternet/mobile/vendamobile/",
  // "URL_JUNO_FINALIZAR_VENDA": "http://mobile-dev.redepos.com.br:8080/pagamento_juno/wizard.html?hash=",
  
  //LOCALHOST
  // "APP_SERVICE" : "http://192.168.56.1/appdasortesvc/",
  // "URL_FINALIZAR_VENDA" : "http://192.168.56.1/appdasortesvc/vendainternet/mobile/vendamobile",
  
  // SERVICOS
  "APP_SERVICE_LOGIN": "login",
  "APP_SERVICE_INICIO": "inicio",
  "APP_SERVICE_PARAMETRO": "parametro",
  "APP_SERVICE_ENVIAR_SENHA": "enviar-senha",
  "APP_SERVICE_ESCOLHER_TITULO": "certificado/post/certificados",
  "APP_SERVICE_CLIENTE": "cliente",
  "APP_SERVICE_CLIENTE_TRANSACOES": "cliente/post/transacoes",
  "APP_SERVICE_CLIENTE_TRANSACOES_PENDENTES": "cliente/post/transacoes-pendente",
  "APP_SERVICE_CLIENTE_TRANSACOES_CANCELADOS": "cliente/post/transacoes-cancelado",
  "APP_SERVICE_CLIENTE_EDICOES": "cliente/post/edicoes-cliente",
  "APP_SERVICE_CLIENTE_ATUALIZAR": "cliente/post/atualizar",
  "APP_SERVICE_TRANSACAO_VENDA_INICIO": "transacao/post/venda-iniciada",
  "APP_SERVICE_TERMO": "termo",
  "APP_SERVICE_REGIAO": "regiao",
  "APP_SERVICE_RESULTADO": "resultado",
  "APP_SERVICE_EDICAO_DATAS_SORTEIO": "resultado/get/datas-sorteio",
  "APP_SERVICE_FIDELIDADE_PRODUTOS": "fidelidade/produtos",
  "APP_SERVICE_FIDELIDADE_EXTRATO": "fidelidade/extrato",
  "APP_SERVICE_UPDATE_PLAYER_ID": "login/post/atualizar-player-id",
  "APP_SERVICE_GANHADORES": "resultado/post/ganhadores",
  
  
  // STATUS
  
  "STATUS_CERTIFICADO_BUSCADO": 0,
  "STATUS_CERTIFICADO_SELECIONADO": 1,
  "STATUS_CERTIFICADO_COMPRADO": 2,
  
  // CONFIG
  
  "CONFIG_MARCAR_TODOS_CERTIFICADOS": "appdasorte.configuracao.marcar_todos_certificados",
  "CONFIG_NOTIFICACAO_INICIO_SORTEIO": "appdasorte.configuracao.notificacao.iniciosorteio"
}
