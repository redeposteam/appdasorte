
import Constants, { BASE_PLACEHOLDER } from './constants';
import { Platform } from 'react-native';
import DeviceInfo from '../util/device'
import NetInfo from "@react-native-community/netinfo";
import axios from "axios";
import Crypto from '../util/crypto';

export const NO_NETWORK = 5;
export const TOKEN_EXPIRED_CODE = "401";
const TIMEOUT = 3000000;
const JSON_HEADER = { 'Content-Type': 'application/json' };

export default class ServiceApi {

    static __instance;
    axiosInstance;
    application;

    static get instance() {
        return new ServiceApi();
        // if (!this.__instance) {
        //     this.__instance = new ServiceApi();
        // }
        // return this.__instance;
    }

    async hasNetworkConnection() {
        const status = await NetInfo.fetch();
        return status.isConnected;
    }

    async getAxiosInstance(headers) {
        if (!this.axiosInstance) {
            this.axiosInstance = axios.create({
                baseURL: Constants.APP_SERVICE,
                timeout: TIMEOUT,
            });
        }

        let deafultHeaders = await this.getHeaders();
        if (headers) {
            deafultHeaders = Object.assign({}, deafultHeaders, headers);
        }

        this.axiosInstance.defaults.headers = deafultHeaders;
        return this.axiosInstance;
    }

    async post(url, data, header) {

        let hasNetworkConnection = await this.hasNetworkConnection();
        if (hasNetworkConnection) {
            let axios = await this.getAxiosInstance(header);

            return axios.post(url, data).then((result) => {
                if (result.data && result.data.codigo && result.data.codigo.includes(TOKEN_EXPIRED_CODE)) {
                    let error = new Error('Sessão expirada. Por favor faça login novamente.');
                    error.code = TOKEN_EXPIRED_CODE;
                    throw error;
                }
                return result;
            }).catch((error) => {
                //TimeOut
                if (error.code === 'ECONNABORTED') {
                    throw new Error(Constants.APP_MSG_TIMEOUT);
                }else{
                    let error = new Error('Sem conexão com a internet.');
                    error.code = NO_NETWORK;
                    throw error;
                }
            });
        } else {
            let error = new Error('Sem conexão com a internet.');
            error.code = NO_NETWORK;
            throw error;
        }
    }

    async requestParameters(base) {
        let data = "base=" + base + "&platform=android";
        if (Platform.OS === 'ios') {
            data = "base=" + base + "&platform=ios";
        }
        return this.post(Constants.APP_SERVICE_PARAMETRO, data);
    }

    async doLogin(login, password, companyId, playerId) {
        let data = {
            cpf: login,
            senha: password,
            id_empresa: companyId,
            player_id: playerId
        }
        console.log(JSON.stringify(data));
        return this.post(Constants.APP_SERVICE_LOGIN, data, JSON_HEADER);
    }

    async forgotPassword(email, idEmpresa) {
        let data = {
            email: email,
            id_empresa: idEmpresa,
        }
        return this.post(Constants.APP_SERVICE_ENVIAR_SENHA, data, JSON_HEADER);
    }

    async registerNewUser(userData) {
        let data = JSON.stringify(userData);
        return this.post(Constants.APP_SERVICE_CLIENTE, data, JSON_HEADER);
    }

    async updateUser(token, data) {
        let header = Object.assign({}, JSON_HEADER, this.getTokenHeader(token));
        return this.post(Constants.APP_SERVICE_CLIENTE_ATUALIZAR, data, header);
    }

    async getNewCertificates(token, editionId) {
        let data = 'id_edicao=' + editionId;
        return this.post(Constants.APP_SERVICE_ESCOLHER_TITULO, data, this.getTokenHeader(token));
    }

    async sellInitialization(token, data) {
        data = "hash=" + Crypto.encrypt(JSON.stringify(data));
        return this.post(Constants.APP_SERVICE_TRANSACAO_VENDA_INICIO, data, this.getTokenHeader(token));
    }

    async getPendingTransactions(token, userId, editionId) {
        const data = "id_cliente=" + userId + "&id_edicao=" + editionId;
        console.log(JSON.stringify(data));
        return this.post(Constants.APP_SERVICE_CLIENTE_TRANSACOES_PENDENTES, data, this.getTokenHeader(token));
    }

    async getConfirmedTransactions(token, userId, editionId) {
        const data = "id_cliente=" + userId + "&id_edicao=" + editionId;
        return this.post(Constants.APP_SERVICE_CLIENTE_TRANSACOES, data, this.getTokenHeader(token));
    }

    async getCanceledTransactions(token, userId, editionId) {
        const data = "id_cliente=" + userId + "&id_edicao=" + editionId;
        return this.post(Constants.APP_SERVICE_CLIENTE_TRANSACOES_CANCELADOS, data, this.getTokenHeader(token));
    }

    async getResultDates(token, idLotteryType) {
        let data = 'id_tipo_loteria=' + idLotteryType;
        let axios = await this.getAxiosInstance();
        return axios.post(Constants.APP_SERVICE_EDICAO_DATAS_SORTEIO, data);
    }

    async getResults(token, idLotteryType, lotteryDate) {
        let data = 'id_tipo_loteria=' + idLotteryType + '&' + 'data_sorteio=' + lotteryDate;
        let axios = await this.getAxiosInstance(this.getTokenHeader(token));
        return axios.post(Constants.APP_SERVICE_RESULTADO, data);
    }

    async getSellPointsData(companyId, latitude, longitude, distance) {

        let bodyFormData = new FormData();
        bodyFormData.append('idempresa', companyId);
        bodyFormData.append('latitude', latitude);
        bodyFormData.append('longitude', longitude);
        bodyFormData.append('distancia', distance);

        return axios({
            method: 'post',
            timeout: TIMEOUT,
            url: Constants.APP_MAPS + 'consulta.php',
            data: bodyFormData,
            config: { headers: { 'Content-Type': 'multipart/form-data' } }
        });
    }

    async getUserEdition(token, userId) {
        const data = "id_cliente=" + userId;
        return this.post(Constants.APP_SERVICE_CLIENTE_EDICOES, data, this.getTokenHeader(token));
    }

    async addressesByLatLng(latitude, longitude, geocodeKey) {
        return axios({
            method: 'get',
            timeout: TIMEOUT,
            url: "https://maps.googleapis.com/maps/api/geocode/json?key=" + geocodeKey + "&sensor=false&latlng=" + latitude + ',' + longitude,
        });
        //https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCoPRz2H_pmN2dBibd8qQ_UPQ1qFyLURFg&sensor=false&latlng=-9.639821, -35.708645

    }

    updatePlayerId(userId, playerId, token) {
        const data = "idUsuario=" + userId + "&playerId=" + playerId;
        return this.post(Constants.APP_SERVICE_UPDATE_PLAYER_ID, data, this.getTokenHeader(token));
    }

    async getWinnersList(base, editionDate) {

        const data = "base=" + base + "&data_edicao=" + editionDate;
        return axios({
            method: 'post',
            timeout: TIMEOUT,
            url: "https://doecap.com.br/resultado/post/ganhadores",
            data: data
        });
    }

    getTokenHeader(token) {
        return token ? { 'AccessToken': token } : null;
    }

    async getHeaders() {
        try {

            let deviceInfo = await DeviceInfo.getDeviceInfo();
            let csrfToken = await Crypto.csrf();

            return {
                'AppVersion': await DeviceInfo.getVersion(),
                'Device': JSON.stringify(deviceInfo),
                'Content-Type': 'application/x-www-form-urlencoded',
                'CsrfToken': csrfToken
            };
        } catch (error) {
            console.log(error);
            throw error;
        }
    }
}
