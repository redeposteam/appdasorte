//oneSignalId: '2b98ac00-cbb1-4d33-90a3-1aa9a6e6f5bd',
getImageURL = (base) => {
  // const imgURL = 'https://www.doecap.com.br/app/img/base/';
  const imgURL = 'https://' + base + '.dasorte.com/app/img/base/';
  return {uri: imgURL + base + '.png'};
};

export default Configurations = {
  feira: {
    appTitle: 'Feira dá Sorte',
    appIcon: require('../resources/img/fe/fe.icon.png'),
    base: 'feira',
    oneSignalId: 'a808ace6-29f9-4cd9-9521-3c8d9d075392',
    geocodingKey: 'AIzaSyCsDgHyKMDbGmZNdGxEPL8A5RVqNKMrAFY',
    splash: {
      splashImg: getImageURL('fe'),
      minSplashTime: 100
    }
  },
  pernambuco: {
    appTitle: 'Pernambuco dá Sorte',
    appIcon: require('../resources/img/pe/pe.icon.png'),
    base: 'pernambuco',
    oneSignalId: '844748f0-10ff-48b8-9094-0a1e1e37089d',
    geocodingKey: 'AIzaSyDaSFIZZ7eU0iJVTlUlaaprFjTz3OV3jjc',
    splash: {
      splashImg: getImageURL('pe'),
      minSplashTime: 100
    },
  },
  bahia: {
    appTitle: 'Bahia dá Sorte',
    appIcon: require('../resources/img/ba/ba.icon.png'),
    oneSignalId: 'd35d3adc-b4b2-4411-916b-9f6f61aa9ad4',
    geocodingKey: 'AIzaSyC1J_JnE9L6hD0r0tQkYPwijR_l7DKT97w',
    base: 'bahia',
    splash: {
      splashImg: getImageURL('ba'),
      minSplashTime: 100
    }
  },
  saopaulo: {
    appTitle: 'São Paulo dá Sorte',
    appIcon: require('../resources/img/sp/sp.icon.png'),
    oneSignalId: '3f51af78-b5f8-43b6-9171-07dd3d9d00fb',
    geocodingKey: 'NjI3MjcyOTYtMWUxNi00ZmE3LWI1N2MtZGVmZTQ2NjVlZWJl',
    base: 'saopaulo',
    splash: {
      splashImg: getImageURL('sp'),
      minSplashTime: 100
    }
  },
  goias: {
    appTitle: 'Goiás dá Sorte',
    appIcon: require('../resources/img/go/go.icon.png'),
    oneSignalId: 'b8e7320e-8a57-4289-a83d-39cc3d3310a6',
    geocodingKey: 'AIzaSyC_rmi1j1R_pxumEQbVU80i06nyoAqX5Vk',
    base: 'goias',
    splash: {
      splashImg: getImageURL('go'),
      minSplashTime: 100
    }
  },
  alagoas: {
    appTitle: 'Alagoas dá Sorte',
    appIcon: require('../resources/img/al/al.icon.png'),
    oneSignalId: 'c27bdfcf-caec-456f-bd0c-738b515a888b',
    geocodingKey: 'AIzaSyCoPRz2H_pmN2dBibd8qQ_UPQ1qFyLURFg',
    base: 'alagoas',
    splash: {
      splashImg: getImageURL('al'),
      minSplashTime: 100
    }
  },
  carimbo: {
    appTitle: 'Carimbó dá Sorte',
    appIcon: require('../resources/img/ca/ca.icon.png'),
    oneSignalId: 'f4b0fa5c-cf4e-442b-bf45-56a186277420',
    geocodingKey: 'AIzaSyCwOQB2RJeIqGZSo09CQ6Pg4CBKc8_ZsOI',
    base: 'carimbo',
    splash: {
      splashImg: getImageURL('ca'),
      minSplashTime: 100
    }
  },
  capixaba: {
    appTitle: 'Capixaba Cap',
    appIcon: require('../resources/img/cp/cp.icon.png'),
    oneSignalId: '3d685792-1358-421f-8ecb-45a2bc36f6fc',
    geocodingKey: 'AIzaSyAhR7DUa2wXQP0MUv7QHmZRWWbicOMgpqY',
    base: 'capixaba',
    splash: {
      splashImg: getImageURL('cp'),
      minSplashTime: 100
    }
  },
  amazonas: {
    appTitle: 'Amazonas dá Sorte',
    appIcon: require('../resources/img/am/am.icon.png'),
    oneSignalId: '14420452-e4a0-4c7a-aa16-e5da647e0503',
    geocodingKey: 'AIzaSyBy3gwLDLFPZflarWdP-XfU5BKg5hD0SF8',
    base: 'amazonas',
    splash: {
      splashImg: getImageURL('am'),
      minSplashTime: 100
    }
  },
  redepos: {
    appTitle: 'RedePos dá Sorte',
    appIcon: require('../resources/img/pe/pe.icon.png'),
    base: 'redepos',
    oneSignalId: '844748f0-10ff-48b8-9094-0a1e1e37089d',
    geocodingKey: 'AIzaSyDaSFIZZ7eU0iJVTlUlaaprFjTz3OV3jjc',
    splash: {
      splashImg: getImageURL('pe'),
      minSplashTime: 100
    },
  },
}