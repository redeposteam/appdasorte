
import Storage, { storageKeys } from '../storage'

export default class CertificateStorage {

    static storeCertificatesData = async (data) => {
        return Storage.storeData(data, storageKeys.CERTIFICATES_API_KEY);
    }

    static retrieveCertificatesData = async () => {
        return Storage.retrieveData(storageKeys.CERTIFICATES_API_KEY);
    }

    static async storeSeletedCertificate(data) {
        return Storage.storeData(data, storageKeys.CERTIFICATES_SELECTED_KEY);
    }

    static async retrieveSeletedCertificates() {
        return Storage.retrieveData(storageKeys.CERTIFICATES_SELECTED_KEY);
    }

    static storeDate = async (date) => {
        return Storage.storeData(date.getTime(), storageKeys.CERTIFICATES_DATE_KEY);
    }

    static retrieveDate = async () => {
        let date = await Storage.retrieveData(storageKeys.CERTIFICATES_DATE_KEY);
        if (date) {
            return new Date(date);
        }
        return null;
    }

    static async removeSelectedCerticatesData() {
        Storage.removeData(storageKeys.CERTIFICATES_SELECTED_KEY);
    }
}