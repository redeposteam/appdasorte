
import ServiceApi from '../../services/serviceApi';
import UserStorage from './userStorage'
import Crypto from '../../util/crypto';

export default class UserController {

    static async doLogin(login, password, params) {

        if (params && params.info && params.info.empresa) {

            let companyId = params.info.empresa.id_empresa;
            let encripPass = Crypto.encrypt(password);
            let playerId = await this.retrievePlayerId();

            return ServiceApi.instance.doLogin(login, encripPass, companyId, playerId)
            .then((result) => {
                if (result.data.success === "true") {
                    UserStorage.storeUserData(result.data.data);
                    return result;
                } else {

                    throw new Error(result.data.message);
                }
            }).catch((error) => {
                console.log(error);
                throw error;
            });
        } else {
            throw new Error('Não foi possível fazer login');
        }
    }

    static async forgotPassword(email, idEmpresa) {
        return ServiceApi.instance.forgotPassword(email, idEmpresa);
    }

    static async updateUser(token, userData, idEmpresa) {
        let data = userData;
        data.id_empresa = idEmpresa;
        if (data.senha) {
            data.senha = Crypto.encrypt(userData.senha);
            data.senha_antiga = Crypto.encrypt(userData.senha_antiga);
            data.senha_confirmacao = Crypto.encrypt(userData.senha_confirmacao);
        }
        return ServiceApi.instance.updateUser(token, data);
    }

    static async storePlayerId(playerId) {
        return UserStorage.storePlayerId(playerId);
    }

    static async retrievePlayerId() {
        return UserStorage.retrievePlayerId();
    }

    static storeUserData(userData) {
        UserStorage.storeUserData(userData);
    }

    static retrieveUsersData = () => {
        return UserStorage.retrieveUserData();
    }

    static removeUserData = ()=> {
        return UserStorage.removeUserData();
    }

    static async initUserData() {
        return UserStorage.initUserData();
    }

    static async registerNewUser(userData) {
        return ServiceApi.instance.registerNewUser(userData);
    }

    static async updatePlayerId(userId, playerId, token) {
        return ServiceApi.instance.updatePlayerId(userId, playerId, token);
    }

}
