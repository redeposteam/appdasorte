import ServiceApi from "../../services/serviceApi";

export default class MarkCertificateController {
    
    static async getUserEdition(token, userId) {
        return ServiceApi.instance.getUserEdition(token, userId);
    }
}