
import BadgeNotificationsStorage from './badgeNotificationsStorage'
import BadgeNotificationsCenter from './badgeNotificationsCenter'
import UserController from '../user/userController'

let NOTIFICATION_ROUTE_ACTION = '';

export default class BadgeNotificationsController {

    static async retrieveBadgeNotificationsData() {
        return await BadgeNotificationsStorage.retrieveBadgeNotificationsData();
    }

    static async retrieveBadgeNotificationsByRouteKey(routeKey) {
        let data = await BadgeNotificationsStorage.retrieveBadgeNotificationsData();
        return data ? data[routeKey] : null;
    }

    static async storeBadgeNotificationsData(routeKey) {
        let badgeNotifications = await this.retrieveBadgeNotificationsData(routeKey);
        if (!badgeNotifications) {
            badgeNotifications = {};
        }
        badgeNotifications[routeKey] = (badgeNotifications[routeKey] ? badgeNotifications[routeKey] : 0) + 1;
        this.setBadgeWithRouteKey(routeKey, badgeNotifications[routeKey]);
        //this.setBadgeForDrawerIcon(badgeNotifications);
        await BadgeNotificationsStorage.storeBadgeNotificationsData(badgeNotifications);
        return;
    }

    static async removeBadgeNotificationData(routeKey) {
        let badgeNotifications = await this.retrieveBadgeNotificationsData();
        if (badgeNotifications && badgeNotifications[routeKey] > 0) {
            badgeNotifications[routeKey] = 0;
            this.setBadgeWithRouteKey(routeKey, 0);
            //this.setBadgeForDrawerIcon(badgeNotifications);
            return BadgeNotificationsStorage.storeBadgeNotificationsData(badgeNotifications);
        }
        return;
    }

    static addDrawerLabelState(routeKey, stateFunc) {
        BadgeNotificationsCenter.instance.addDrawerLabelState(routeKey, stateFunc);
    }

    static addDrawerIconState(stateFunc) {
        BadgeNotificationsCenter.instance.addDrawerIconState(stateFunc);
    }

    static setBadgeWithRouteKey(routeKey, newBadge) {
        BadgeNotificationsCenter.instance.setBadgeWithRouteKey(routeKey, newBadge);
    }

    static async setBadgeForDrawerIcon(badgeData) {
        //let newBadge = await this.retrieveBadgeNotificationsForDrawerIcon(badgeData);
        if (badgeData) {
            const newBadge = Object.values(badgeData).reduce((a, b) => a + b, 0);
            BadgeNotificationsCenter.instance.setBadgeForDrawerIcon(newBadge);
        }
    }

    static async retrieveBadgeNotificationsForDrawerIcon(data) {
        if (!data) {
            data = await this.retrieveBadgeNotificationsData()
        }
        let userData = await UserController.retrieveUsersData();
        let total = 0;
        if (userData) {
            for (const key in data) {
                total = total + data[key];
            }
        }
        return total;
    }

    static setNotificationRouteAction(routeKey) {
        NOTIFICATION_ROUTE_ACTION = routeKey;
        this.removeBadgeNotificationData(routeKey);
    }

    static getNotificationRouteAction() {
        return NOTIFICATION_ROUTE_ACTION;
    }
}