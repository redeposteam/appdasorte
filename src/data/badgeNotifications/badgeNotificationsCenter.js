
export default class BadgeStateCenter {

    static get instance() {
        if (!this.__instance) {
            this.__instance = new BadgeStateCenter();
        }
        return this.__instance;
    }

    constructor() {
        this.statesFunctions = {};
    }

    addDrawerLabelState(routeKey, setStateFunc) {
        this.statesFunctions[routeKey] = setStateFunc;
    }

    addDrawerIconState(setStateFunc) {
        this.drawerIconFunction = setStateFunc;
    }

    setBadgeWithRouteKey(routeKey, newBadge) {
        if (this.statesFunctions[routeKey]) {
            let func = this.statesFunctions[routeKey];
            func({ badge: newBadge });
        }
    }

    setBadgeForDrawerIcon(newBadge) {
        if (this.drawerIconFunction) {
            //alert('DrawerIcon');
            this.drawerIconFunction({ badge: newBadge });
        }
    }
}