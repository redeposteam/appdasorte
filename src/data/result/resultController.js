
import ServiceApi from '../../services/serviceApi'

export default class ResultController {

    static async getResultDates(token, idLotteryType) {        
        return ServiceApi.instance.getResultDates(token, idLotteryType);
    }

    static async getResults(token, idLotteryType, lotteryDate) {
        return ServiceApi.instance.getResults(token, idLotteryType, lotteryDate);
    }

    static getWinnersList(base, editionDate) {
        
        const arr = editionDate.split('/');
        const sep = '-';
        let date = arr[2] + sep + arr[1] + sep + arr[0];
        // alert(date);
        // date = '2019-04-07';
        
        return ServiceApi.instance.getWinnersList(base, date);
    }
}