
import NetInfo from "@react-native-community/netinfo";
var CryptoJS = require("crypto-js");

export default class Crypto {

    static encryptAES = function (key, iv, text) {

        key = CryptoJS.enc.Utf8.parse(CryptoJS.MD5(key));
        iv = CryptoJS.enc.Utf8.parse(iv);
        encrypted = CryptoJS.AES.encrypt(text, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.ZeroPadding });
        return encrypted.ciphertext.toString(CryptoJS.enc.Base64);
    }

    static decryptAES = function (key, iv, text) {

        key = CryptoJS.enc.Utf8.parse(CryptoJS.MD5(key));
        iv = CryptoJS.enc.Utf8.parse(iv);
        decrypted = CryptoJS.AES.decrypt(text, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.ZeroPadding });
        return decrypted.toString(CryptoJS.enc.Utf8);
    }

    static encrypt = function (texto) {
        var hashs = this.hash().split('-');
        return this.encryptAES(hashs[0], hashs[1], texto);
    }

    static decrypt = function (texto) {
        var hashs = this.hash().split('-');
        return Util.decryptAES(hashs[0], hashs[1], texto);
    }

    static csrf = async function () {
        const status = await NetInfo.fetch();
        let ip = "";
        if (status.details) {
            if (status.type === 'wifi') {
                ip = status.details.ipAddress;
            }
        }
        //console.log('asd asd' + JSON.stringify(status.details.ipAddress));
        var hashs = this.hash().split('-');
        var csrf = ':' + ip + ':' + parseInt((new Date().getTime() / 1000));
        return this.encryptAES(hashs[0], hashs[1], csrf);
    }

    static hash() {
        key = '';
        for (i = 0; i < 16; i++) {
            if (i == 0) {
                key += 2 * 4;
            }
            if (i == 1) {
                key += (2 * 4) - i;
            }
            if (i == 2) {
                key += 3 * i;
            }
            if (i == 3) {
                key += 2 + i;
            }
            if (i == 4) {
                key += 1 * i;
            }
            if (i == 5) {
                key += i - 2;
            }
            if (i == 6) {
                key += 6 / 3;
            }
            if (i == 7) {
                key += 2 * 4 - i;
            }
            if (i == 8) {
                key += i;
            }
            if (i == 9) {
                key += i - 2;
            }
            if (i == 10) {
                key += (i / 2) + 1;
            }
            if (i == 11) {
                key += i - (2 * 3);
            }
            if (i == 12) {
                key += i / 3;
            }
            if (i == 13) {
                key += ((i - 1) / 3) - 1;
            }
            if (i == 14) {
                key += i / 7;
            }
            if (i == 15) {
                key += i / i;
            }
        }
        iv = '';
        for (i = 0; i < 16; i++) {
            if (i == 0) {
                iv += 1 + i;
            }
            if (i == 1) {
                iv += 2 * i;
            }
            if (i == 2) {
                iv += 1 * i;
            }
            if (i == 3) {
                iv += 1 + i;
            }
            if (i == 4) {
                iv += 1 + i;
            }
            if (i == 5) {
                iv += i - 2;
            }
            if (i == 6) {
                iv += 6 / 3;
            }
            if (i == 7) {
                iv += 2 * 4 - i;
            }
            if (i == 8) {
                iv += i;
            }
            if (i == 9) {
                iv += i - 2;
            }
            if (i == 10) {
                iv += (i / 2) + 1;
            }
            if (i == 11) {
                iv += i - (2 * 3);
            }
            if (i == 12) {
                iv += i / 3;
            }
            if (i == 13) {
                iv += ((i - 1) / 3) - 1;
            }
            if (i == 14) {
                iv += i / 7;
            }
            if (i == 15) {
                iv += i / i;
            }
        }
        return key + '-' + iv;
    }
}