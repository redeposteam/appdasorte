
import { PermissionsAndroid, Platform } from 'react-native';
import Geolocation from 'react-native-geolocation-service';

export default class GeolocationInfo {

    static async getCurrentPosition() {

        try {
            if (Platform.OS === 'android') {
                let permissionGranted = await this.requestLocationPermission();
                if (permissionGranted) {
                    return this.requestLocation();
                } else {
                    return null;
                }
            } else {
                return this.requestLocation();
            }
        } catch (error) {
            throw error;
        }
    }

    static requestLocation() {
        return new Promise((resolve, reject) => {
            Geolocation.getCurrentPosition((location) => {
                resolve(location);
            }, (error) => {
                resolve(null);
            }, { enableHighAccuracy: true, timeout: 10000 });
        })
    }

    static async requestLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                {
                    title: 'Permissão',
                    message: 'Permitir o uso de sua localização?',
                    buttonNegative: "Negar",
                    buttonPositive: "Sim"
                }
            )
            return granted === PermissionsAndroid.RESULTS.GRANTED;
        } catch (error) {
            console.log(error);
            throw error;
        }
    }
}