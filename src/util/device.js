
import Device from 'react-native-device-info';
import SimInfo from "./simInfo";
import GeolocationInfo from "../util/geolocation";

export default class DeviceInfo {

    static async getDeviceInfo() {
        // let geo = await GeolocationInfo.getCurrentPosition();
        // let coords = null;
        // if (geo && geo.coords && geo.coords.latitude) {
        //     coords = {
        //         'latitude': geo.coords.latitude,
        //         'longitude': geo.coords.longitude
        //     }
        // }

        return {
            'uuid': Device.getDeviceId(),
            'serial': await Device.getSerialNumber(),
            'model': Device.getModel(),
            'manufacturer': await Device.getManufacturer(),
            'platform': Device.getSystemName(),
            'version': Device.getSystemVersion(),
            'sim': await SimInfo.getSimInfo(),
            //'geo': coords
        }
    }

    static async getSystemName() {
        return await Device.getSystemName();
    }

    static async getVersion() {
        return await Device.getVersion();
    }
}