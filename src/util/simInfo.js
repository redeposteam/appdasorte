
import CarrierInfo from 'react-native-carrier-info';

export default class SimInfo {

    static async getSimInfo() {                

        return {
            'carriername': await CarrierInfo.carrierName().catch(() => ''),
            'countrycode': await CarrierInfo.isoCountryCode().catch(() => ''),
            'mcc': await CarrierInfo.mobileCountryCode().catch(() => ''),
            'mnc': await CarrierInfo.mobileNetworkCode().catch(() => ''),
            //'callstate': CarrierInfo.mobileNetworkCode(),
            //'dataactivity': CarrierInfo.getDataActivity(), 
            //'networktype': CarrierInfo.getNetworkType(), 
            //'phonetype': CarrierInfo.getPhoneType(), 
            //'simstate': CarrierInfo.getSimState(), 
            //'isnetworkroaming': CarrierInfo.getIsNetworkRoaming(), 
            //'phonecount': CarrierInfo.getPhoneCount()
        }        
    }

}