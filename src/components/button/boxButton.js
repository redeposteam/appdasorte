
import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import Colors from '../../resources/colors'
import Dimens from '../../resources/dimens';
import withPreventDoubleClick from '../../util/withPreventDoubleClick'

const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

export default class BoxButton extends Component {

    render() {

        let content = () => {
            if (this.props.text) {
                return <Text style={styles.textButton}>{this.props.text}</Text>
            }
            return this.props.children;
        }

        return (
            <TouchableOpacityEx style={[styles.button, {
                backgroundColor: this.props.color ? this.props.color : Colors.positiveGreen,
                width: this.props.width ? this.props.width : '100%',
            }, this.props.style]} onPress={() => {
                if (this.props.action) this.props.action();
            }}>
                {content()}
            </TouchableOpacityEx>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        padding: Dimens.defaultPadding,
        backgroundColor: Colors.positiveGreen,
        borderRadius: 5,
        width: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    textButton: {
        color: 'white',
        alignSelf: 'center',
        fontWeight: 'bold'
    }
});
