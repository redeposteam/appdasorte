import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Colors from '../../resources/colors';
import withPreventDoubleClick from '../../util/withPreventDoubleClick';


const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

export default class LinkButton extends Component {
    render() {

        let floatDirection = this.props.floatDirection ? this.props.floatDirection : 'center';
        let styles = { textAlign: floatDirection, color: this.props.color ? this.props.color : Colors.linkBlue, fontSize: 18 };

        return (
            <TouchableOpacityEx onPress={() => {
                if (this.props.action) this.props.action();
            }}>
                <Text style={styles}>{this.props.text}</Text>
            </TouchableOpacityEx>
        );
    }
}