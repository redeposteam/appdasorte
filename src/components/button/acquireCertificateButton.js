import React, { Component } from 'react';
import { Linking, Platform, View } from 'react-native'
import BoxButton from './boxButton'
import Colors from '../../resources/colors';
import Application from "../../appConfig/application";

export default class AcquireCertificateButton extends Component {

    render() {
        let modality = Application.instance.modality;
        let textBoxButton = 'ADQUIRIR '+modality.toLocaleUpperCase();
        return (
            <BoxButton
                style={{ width: '90%' }}
                text={textBoxButton}
                color={Colors.linkBlue} action={() => {
                    this.props.navigate('BuyCertification');
                }} />
        )
    }
}
