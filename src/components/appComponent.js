
import { Alert, Linking, Dimensions, StatusBar, InteractionManager, Platform } from 'react-native'
import { Component } from 'react'
import AppHeader from './screens/main/appHeader'
import Application, { AuditType } from '../appConfig/application'
import Preferences from '../data/preferences/preferences'
import Orientation from 'react-native-orientation'
import { Header } from 'react-navigation-stack'
import FabricLogs from '../util/fabricLogs';

const initialDimensions = { width, height } = Dimensions.get('window');
const initialOrientation = Orientation.getInitialOrientation();
export const SequentialType = {
    None: 0,
    Child: 1,
    Parent: 2,
}

export default class AppComponent extends Component {

    state = {
        hideLoading: true
    }

    constructor() {
        super();
        this.setDimensions();
        this.toolbarHeight = Header.HEIGHT;
        this.application = Application.instance;
        this.config = this.application.configurations;
        this.params = this.application.parameters;
        this.userData = this.application.userData;
        this.usertoken = this.userData ? this.userData.token : null;
        this.mainColor = this.application.parameters ? this.application.parameters.css.strCorBarra : 'gray';
        this.Preferences = Preferences;
        this.hasEdition = this.params.info && this.params.info.edicao_atual;
        if (this.params.info && this.params.info.edicao_atual && this.params.info.edicao_atual.produtos
            && this.params.info.edicao_atual.produtos instanceof Array) {
            this.isTabuled = this.params.info.edicao_atual.produtos[0].tipo === '2';
            this.isGiroDaSorte = this.params.info.edicao_atual.sorteios[0].tipo_sorteio === '2';
            this.setSequentialType(this.isGiroDaSorte);
        }
        if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(this.mainColor);
        }
        StatusBar.setBarStyle('light-content');
    }

    setSequentialType(isGiroDaSorte) {
        this.sequentialType = SequentialType.None;
        if (isGiroDaSorte) {
            this.sequentialType = this.params.info.auditoria === AuditType.Aplub ?
                SequentialType.Child : SequentialType.Parent;
        }
    }

    componentDidMount() {
        if (this.props.navigation) {
            let userData = this.getUsuario();
            Application.logContentView(this.props.navigation.state.routeName, userData);
        }
        this.registerOrientationListener();
    }

    getUsuario() {
        return this.userData ? this.userData.usuario : null;
    }

    componentWillUnmount() {
        this.unmounted = true;
        Orientation.removeOrientationListener(this.orientationDidChange);
    }

    setState(state, func) {
        if (this.unmounted) {
            return;
        }
        super.setState(state, func);
    }

    isEditionValid() {
        return this.params.info !== null && this.params.info.edicao_atual !== null;
    }

    setDimensions() {
        this.dimensions = initialDimensions;
        if (initialOrientation === 'LANDSCAPE') {
            this.dimensions = { width: this.dimensions.height, height: this.dimensions.width };
        }
    }

    registerOrientationListener() {
        Orientation.addOrientationListener(this.orientationDidChange.bind(this));
    }

    afterAnimations() {
    }

    orientationDidChange(orientation) {
    }

    getOrientation(func) {
        Orientation.getOrientation((err, orientation) => {
            func(err, orientation)
        });
    }

    lockToPortrait() {
        //Orientation.lockToPortrait();
    }

    unlockAllOrientations() {
        //Orientation.unlockAllOrientations();
    }

    getDimensionsByOrientation(func) {
        this.getOrientation((err, orientation) => {
            func(this.getDimensions(orientation));
        });
    }

    getDimensions(orientation) {
        if (!orientation) {
            return this.dimensions;
        }
        if (initialOrientation !== orientation) {
            return { width: this.dimensions.height, height: this.dimensions.width };
        }
        return this.dimensions;
    }

    showAlert(title, msg) {
        Alert.alert(title, msg);
    }

    showLoading() {
        this.setState({ hideLoading: false });
    }

    hideLoading() {
        this.setState({ hideLoading: true });
    }

    updateUserName() {
        if (this.userData) {

        }
    }

    openUrl(url) {
        Linking.canOpenURL(url)
            .then(supported => {
                if (supported) {
                    Linking.openURL(url);
                } else {
                    console.log("Don't know how to open URI: " + this.props.url);
                }
            });
    }

    static navigationOptions = ({ navigation }) => {
        return AppHeader.loadCssParameters(navigation, Application.instance.parameters);
    }

    static getNavigationOptions = (nav, title, checkoutAction) => {
        let _title = title ? title : Application.instance.configurations.appTitle;
        return AppHeader.loadCssParameters(nav, Application.instance.parameters, _title, checkoutAction);
    }

    static onTopNavigationOptions = (barColor, title, navigation) => {
        return AppHeader.onTopNavigationOptions(barColor, title, navigation);
    }

    static profileHeaderRight = (navigation, title) => {
        return AppHeader.profileHeaderRight(navigation, title);
    }

    static checkoutScreenHeader = (navigation, title, leftButton) => {
        return AppHeader.checkoutScreenHeader(navigation, Application.instance.parameters, title, leftButton);
    }

    logMyErrors(error) {
        console.log(error);
        if (error && error.code === this.application.TokenExpiredCode) {
            this.application.logout();
            this.props.navigation.navigate('Profile', { barColor: this.mainColor });
            if (error && error.message) {
                alert(error.message);
            }
        } else {
            Application.logError(error, this.userData);
        }
    }

    logAddToCart(value, itemType, itemName, itemId) {
        Application.logAddToCart(value, itemType, itemName, itemId, this.getUsuario());
    }

    crashTest() {
        Application.crashTest();
    }
}