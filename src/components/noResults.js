
import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import constants from '../services/constants';

export default class NoResult extends Component {

    render() {

        let defaultMsg = this.props.hasEdition ? constants.APP_MSG_TIMEOUT : constants.APP_MSG_SEM_EDICAO;
    let errorMsg = this.props.errorMsg !== undefined ? this.props.errorMsg : defaultMsg;

        return (
            <View style={styles.main}>
                <Text style={styles.errorText}>{errorMsg}</Text>                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },    
    errorText: {
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold',
        padding: 10
    }
});