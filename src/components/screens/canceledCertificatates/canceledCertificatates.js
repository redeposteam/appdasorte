import React from 'react'
import {View, Text, FlatList} from 'react-native'
import AppComponent from '../../appComponent'
import TransactionsController from '../../../data/transactionsController/transactionsController'
import Loading from '../../loading/loading'
import Colors from '../../../resources/colors'
import LinearGradient from 'react-native-linear-gradient'
import Card from '../../lottery/card'
import DivisorElement from '../../lottery/divisorElement'
import NoCertificate from '../../noCertificate'
import {NavigationEvents} from 'react-navigation'
import Application from '../../../appConfig/application';

// const json = {
//     data: {
//         "success": "true",
//         "data": [
//             {
//                 "valor": 5,
//                 "tipo": null,
//                 "id_edicao": "7520",
//                 "id_transacao": "2A442040-3006-4E56-AE0C-8D34D6849100",
//                 "status_cor": "#48b14c",
//                 "status_transacao": "Paga",
//                 "data_hora": "25/01/2019 14:57:35",
//                 "data_sorteio": "27/01/2019",
//                 "data_hora_sorteio": "27/01/2019 10:30:00",
//                 "numero": "027.0.451.332-07",
//                 "autorizacao": "15414.901222/2017-11|15414.901221/2017-69",
//                 "multiplicador": "1",
//                 "imagem": "https://www.doecap.com.br/index.php/../app/img/edicao/7520.jpg",
//                 "imagemmobile": "https://www.doecap.com.br/index.php/../app/img/edicao/7520-mobile.jpg",
//                 "vendas": [
//                     {
//                         "id_venda": 167485179,
//                         "certificados": [
//                             {
//                                 "sequencial": "451332",
//                                 "numero": "027.0.451.332-07",
//                                 "valor": "5",
//                                 "itens": [
//                                     {
//                                         "sequencial": "451332",
//                                         "numero": "027.0.451.332-07",
//                                         "dezenas": "06-07-08-17-19-20-23-28-30-31-32-35-36-44-45-47-49-55-59-60",
//                                         "valor": "5"
//                                     }
//                                 ]
//                             }
//                         ],
//                         "giro": 1
//                     }
//                 ]
//             }
//         ]
//     }
// }

export default class CanceledCertificatates extends AppComponent {

    state = {
        data: null,
        showLoading: true
    }

    onWillFocus = () => {
        this.setState({showLoading: true});
        super.componentDidMount();
        if (this.isEditionValid()) {
            this.loadCanceledTransactions();
        } else {
            this.setState({showLoading: false});
        }
    }

    async loadCanceledTransactions() {

        // let result = json;
        // if (result.data.success === 'true') {
        //     this.setState({ data: result.data.data, showLoading: false });
        // } else {
        //     this.setState({ showLoading: false });
        // }

        TransactionsController.getCanceledTransactions(this.usertoken, this.userData.usuario.id, this.params.info.edicao_atual.id)
            .then((result) => {
                if (result.data.success === 'true' && result.data.data.length > 0) {
                    this.setState({data: result.data.data, showLoading: false});
                } else {
                    this.setState({showLoading: false});
                }
            }).catch((error) => {
            console.log(error);
            this.setState({showLoading: false});
            this.logMyErrors(error);
        });
    }

    renderItem({item, index}) {

        let titleStyle = {fontSize: 15, fontWeight: 'bold',};
        let descStyle = {fontSize: 13, fontWeight: 'normal'};
        let textModality = Application.instance.modality + ':';
        return (
            <LinearGradient colors={['white', 'white', Colors.defaultBackground]}
                            style={{paddingHorizontal: 10, paddingBottom: 10,}}>
                <Card>
                    {/* <Image style={{ width: '100%', height: 100 }} resizeMode='contain' source={{ uri: this.props.banner }} /> */}
                    <View style={{padding: 10,}}>
                        <DivisorElement title={'Transação: ' + item.id_transacao}/>
                        <View style={{flexDirection: 'row', justifyContent: 'center',}}>
                            <View style={{padding: 5}}>
                                <Text style={titleStyle}>Situação: <Text style={[descStyle, {
                                    fontWeight: 'bold',
                                    color: item.status_cor
                                }]}>{item.status_transacao}</Text></Text>
                                <Text style={titleStyle}>Sorteio: <Text
                                    style={descStyle}>{item.data_hora_sorteio}</Text></Text>
                            </View>
                            <View style={{padding: 5}}>
                                <Text style={titleStyle}> {textModality} <Text
                                    style={descStyle}>{item.quantidade}</Text></Text>
                                <Text style={titleStyle}>Valor: <Text style={[descStyle, {
                                    fontWeight: 'bold',
                                    color: 'green'
                                }]}>{' R$ ' + Number.parseFloat(item.valor).toFixed(2)}</Text></Text>
                            </View>
                        </View>
                    </View>
                </Card>
            </LinearGradient>
        )
    }

    render() {
        if (this.state.showLoading) {
            return <View style={{flex: 1}}>
                <NavigationEvents
                    onWillFocus={() => {
                        this.onWillFocus()
                    }}/>
                <Loading loadColor={this.mainColor} defaultText hide={false}/>
            </View>
        }

        if (this.state.data) {
            return (
                <View style={{flex: 1, backgroundColor: Colors.defaultBackground}}>
                    <NavigationEvents
                        onWillFocus={() => {
                            this.onWillFocus()
                        }}/>
                    <FlatList
                        data={this.state.data}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}/>
                </View>
            )
        } else {
            let textModality = 'Você não possui ' + this.application.modality + ' cancelados.';
            return (
                <View style={{flex: 1}}>
                    <NavigationEvents
                        onWillFocus={() => {
                            this.onWillFocus()
                        }}/>
                    <NoCertificate text={textModality} navigation={this.props.navigation}/>
                </View>
            )
        }
    }
}
