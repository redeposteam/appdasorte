import React, {PureComponent} from 'react'
import {View, Text, FlatList, TouchableOpacity, RefreshControl} from 'react-native'
import AppComponent from '../../appComponent'
import LinearGradient from 'react-native-linear-gradient'
import Card from '../../lottery/card'
import DivisorElement from '../../lottery/divisorElement'
import TransactionsController from '../../../data/transactionsController/transactionsController'
import Loading from '../../loading/loading'
import NoCertificate from '../../noCertificate'
import Colors from '../../../resources/colors'
import withPreventDoubleClick from '../../../util/withPreventDoubleClick'
import FitImage from 'react-native-fit-image'
import {NavigationEvents} from 'react-navigation';
import Application from "../../../appConfig/application";

const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

class CertificateItem extends PureComponent {

    render() {
        let textModality = Application.instance.modality + ':';
        let item = this.props.item;
        let titleStyle = {fontSize: 13, fontWeight: 'bold',};
        let descStyle = {fontSize: 13, fontWeight: 'normal'};
        let nav = this.props.navigation;
        let isPendingTransaction = this.props.isPendingTransaction;

        return (
            <TouchableOpacityEx onPress={() => {
                setTimeout(() => {
                    nav.navigate('CertificatatesTransactionsDetail', {
                        sells: item.vendas,
                        lotteryDate: item.data_sorteio,
                        isPendingTransaction: isPendingTransaction
                    });
                }, 10);
            }}>
                <LinearGradient colors={['white', 'white', Colors.defaultBackground]}
                                style={{paddingHorizontal: 10, paddingBottom: 10,}}>
                    <Card>
                        <FitImage style={{width: '100%', height: 100}} source={{uri: this.props.banner}}/>
                        {/* <Image style={{ width: '100%', height: 100 }} resizeMode='contain' source={{ uri: this.props.banner }} /> */}
                        <View style={{padding: 10}}>
                            <DivisorElement title={'Transação: ' + item.id_transacao}/>
                            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                <View style={{padding: 5}}>
                                    {/* <Text style={titleStyle}>Transação: <Text style={[descStyle, { fontWeight: 'bold', color: item.status_cor }]}>{item.id_transacao}</Text></Text> */}
                                    <Text style={titleStyle}>Situação: <Text style={[descStyle, {
                                        fontWeight: 'bold',
                                        color: item.status_cor
                                    }]}>{item.status_transacao}</Text></Text>
                                    <Text style={titleStyle}>Data Hora: <Text
                                        style={descStyle}>{isPendingTransaction ? item.data_hora_criacao : item.data_hora}</Text></Text>
                                    <Text style={titleStyle}>Data Hora Sorteio: <Text
                                        style={descStyle}>{item.data_hora_sorteio}</Text></Text>
                                </View>
                                <View style={{padding: 5}}>
                                    <Text style={titleStyle}>{textModality} <Text
                                        style={descStyle}>{item.vendas.length}</Text></Text>
                                    <Text style={titleStyle}>Valor: <Text style={[descStyle, {
                                        fontWeight: 'bold',
                                        color: 'green'
                                    }]}>{' R$ ' + Number.parseFloat(item.valor).toFixed(2)}</Text></Text>
                                </View>
                            </View>
                        </View>
                    </Card>
                </LinearGradient>
            </TouchableOpacityEx>
        )
    }
}

export default class CertificatatesTransactions extends AppComponent {

    state = {
        data: null,
        showLoading: true,
        refreshing: false
    }

    onWillFocus = () => {
        super.componentDidMount();
        this.setState({showLoading: true});
        if (this.isEditionValid()) {
            this.loadPendingCertificates();
        } else {
            this.setState({showLoading: false});
        }
    }

    componentWillUnmount() {
        console.log('componentWillUnmount');
    }

    loadPendingCertificates() {
        let route = this.props.navigation.state.routeName;
        this.isPendingTransaction = route === 'PendingCertificatates';
        if (this.isPendingTransaction) {
            this.getPendingTransactions();
        } else {
            this.getConfirmedTransactions();
        }
    }

    getPendingTransactions() {
        // this.successResponse(json);
        // return;
        TransactionsController.getPendingTransactions(this.usertoken, this.userData.usuario.id, this.params.info.edicao_atual.id)
            .then((result) => {
                this.successResponse(result);
            }).catch((error) => {
            this.errorResponse(error);
        });
    }

    getConfirmedTransactions() {
        // this.successResponse(json);
        // return;
        TransactionsController.getConfirmedTransactions(this.usertoken, this.userData.usuario.id, this.params.info.edicao_atual.id)
            .then((result) => {
                //alert(result)
                this.successResponse(result);
            }).catch((error) => {
            this.errorResponse(error);
        });
    }

    successResponse(result) {
        // this.setState({ data: json.data.data, showLoading: false });
        // return;
        if (result.data.success === 'true' && result.data.data && result.data.data.length > 0) {
            //alert(JSON.stringify(result));
            this.setState({data: result.data.data, showLoading: false});
        } else {
            this.setState({data: null, showLoading: false});
        }
    }

    errorResponse(error) {
        this.logMyErrors(error);
        this.setState({showLoading: false});
    }

    renderItem = ({item, index}) => {
        return <CertificateItem
            key={index}
            item={item}
            isPendingTransaction={this.isPendingTransaction}
            banner={item.imagem}
            navigation={this.props.navigation}/>
    }

    render() {

        if (this.state.showLoading) {
            return (
                <View style={{flex: 1}}>
                    <NavigationEvents
                        onWillFocus={() => {
                            this.onWillFocus()
                        }}/>
                    <Loading loadColor={this.mainColor} defaultText hide={false}/>
                </View>
            )
        }
        if (this.state.data) {
            return (
                <View style={{flex: 1, backgroundColor: Colors.defaultBackground}}>
                    <NavigationEvents
                        onWillFocus={() => {
                            this.onWillFocus()
                        }}/>
                    <FlatList
                        data={this.state.data}
                        renderItem={this.renderItem.bind(this)}
                        keyExtractor={(item, index) => index.toString()}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => {
                                    this.loadPendingCertificates();
                                }}
                                title="Puxe para atualizar"
                                tintColor={this.mainColor}
                                titleColor={this.mainColor}
                                colors={[this.mainColor]}
                            />
                        }
                    />
                </View>
            )
        } else {
            let textModality = this.isPendingTransaction ? 'Você não possui ' + this.application.modality + ' pendentes.' : 'Você não possui ' + this.application.modality + ' confirmados.';
            return (
                <View style={{flex: 1}}>
                    <NavigationEvents
                        onWillFocus={() => {
                            this.onWillFocus()
                        }}/>
                    <NoCertificate text={textModality} navigation={this.props.navigation}/>
                </View>
            )
        }
    }
}

const json = {
    data: {
        "success": "true",
        "data": [
            {
                "valor": 5,
                "tipo": null,
                "id_edicao": "7520",
                "id_transacao": "2A442040-3006-4E56-AE0C-8D34D6849100",
                "status_cor": "#48b14c",
                "status_transacao": "Paga",
                "data_hora": "25/01/2019 14:57:35",
                "data_sorteio": "27/01/2019",
                "data_hora_sorteio": "27/01/2019 10:30:00",
                "numero": "027.0.451.332-07",
                "autorizacao": "15414.901222/2017-11|15414.901221/2017-69",
                "multiplicador": "1",
                "imagem": "https://www.doecap.com.br/index.php/../app/img/edicao/7520.jpg",
                "imagemmobile": "https://www.doecap.com.br/index.php/../app/img/edicao/7520-mobile.jpg",
                "vendas": [
                    {
                        "id_venda": 167485179,
                        "certificados": [
                            {
                                "sequencial": "451332",
                                "numero": "027.0.451.332-07",
                                "valor": "5",
                                "itens": [
                                    {
                                        "sequencial": "451332",
                                        "numero": "027.0.451.332-07",
                                        "dezenas": "06-07-08-17-19-20-23-28-30-31-32-35-36-44-45-47-49-55-59-60",
                                        "valor": "5"
                                    }
                                ]
                            }
                        ],
                        "giro": 1
                    }
                ]
            }
        ]
    }
}
