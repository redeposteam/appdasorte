import React, { Component } from 'react'
import { View, StyleSheet, Text, ScrollView, InteractionManager } from 'react-native'
import HTML from 'react-native-render-html'
import Swiper from 'react-native-swiper'
import Loading from '../../loading/loading'
import Colors from '../../../resources/colors'

const BOX_PADDING = 10;
const BOX_BORDER = 1;
const BOX_FONT_SIZE = 18;
const TIPO_SORTEIO_GIRO = '2';

class PrizeBox extends Component {

    render() {

        return (
            <View style={{
                backgroundColor: 'white',
                borderColor: Colors.defaultBackground,
                borderBottomWidth: BOX_BORDER,
                padding: BOX_PADDING,
                flexDirection: 'row'
            }}>
                <View style={{ justifyContent: 'center', flex: 1, }}>
                    <HTML html={"<span style='font-weight: bold; width:100%; color: #616161; text-align:center; font-size: " + BOX_FONT_SIZE + ";'>" + this.props.value + "</span>"} />
                </View>
            </View>
        );
    }
}

export default class PrizeContainer extends Component {

    state = {
        content: null,
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.loadViews().then(() => this.setState({ content: this.content }));
        });
    }

    async loadViews() {

        let lotteries = this.props.params.info.edicao_atual.sorteios;
        if (lotteries.length > 0) {
            if (this.props.isTabuled) {

                let prizeBoxes = {};
                let index2 = 0;

                lotteries.map((prize, index) => {
                    for (const key in prize.sorteio_premiacoes) {

                        let prize2 = prize.sorteio_premiacoes[key];
                        let value = prize2.valor;
                        let text = (parseInt(prize.numero) + 1) + '° Prêmio - ' + prize2.descricao_premio;

                        if (!prizeBoxes[value]) {
                            prizeBoxes[value] = [];
                        }
                        prizeBoxes[value].push(<PrizeBox key={index2} value={text} />);
                        index2 += 1;
                    }
                });

                let viewsContent = [];
                for (const key in prizeBoxes) {
                    let box = prizeBoxes[key];
                    viewsContent.push(
                        <View key={key}>
                            <View style={[styles.button, { backgroundColor: this.props.mainColor }]}>
                                <Text style={styles.textButton}>{'R$ ' + parseFloat(key).toFixed(2)}</Text>
                            </View>
                            {box}
                        </View>
                    )
                }

                this.content = (
                    <Swiper showsButtons={false}>
                        <ScrollView>
                            {viewsContent}
                        </ScrollView>
                    </Swiper>
                );

            } else {
                this.content = (
                    <ScrollView>
                        {lotteries.map((prize, index) => {
                            if (prize.tipo_sorteio === TIPO_SORTEIO_GIRO) {
                                return (<PrizeBox key={index} value={prize.descricao_premio} />)
                            } else {
                                let desc = prize.numero + '° - Prêmio - ' + prize.descricao_premio;
                                return (<PrizeBox key={index} value={desc} />)
                            }
                        })}
                    </ScrollView>
                )
            }
        }

    }

    render() {
        if (this.state.content) {
            return this.content;
        }
        return <Loading defaultText hide={this.state.hideLoading} />
    }
}

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        flex: 1,
        backgroundColor: 'white'
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    button: {
        padding: BOX_PADDING,
        backgroundColor: Colors.linkBlue,
        width: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    textButton: {
        color: 'white',
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 25
    }
})