import React, { Component } from 'react'
import { createStackNavigator } from 'react-navigation-stack';
import { View, Text, StyleSheet } from 'react-native'
import Result from '../result/result'
import HomeScreen from '../home/home'
import CertificationMap from '../certificationMap/certificationMap'
import BuyCertification from '../buyCertification/buyCertification'
import CheckoutScreen from '../buyCertification/checkoutScreen'
import Terms from '../terms/terms'
import MarkCertificatates from '../markCertificatates/markCertificatates'
import CertificatatesTransactions from '../certificatatesTransactions/certificatatesTransactions'
import CanceledCertificatates from '../canceledCertificatates/canceledCertificatates'
import PrizeScreen from '../buyCertification/prizeScreen'
import Winners from '../winners/winners'
import { Icon } from 'react-native-elements'
import Colors from '../../../resources/colors'
import CertificatatesTransactionsDetail from '../certificatatesTransactions/certificatatesTransactionsDetail'
import BadgeNotificationsController from '../../../data/badgeNotifications/badgeNotificationsController'
import Badge from './badge'
import TermsDetail from '../terms/termsDetail'
import CheckoutWebView from '../buyCertification/checkoutWebView';
import Application from "../../../appConfig/application";

export class DrawerLabel extends Component {

    state = {
        badge: null,
        tintColor: null
    }

    static getDerivedStateFromProps(props, state) {
        if (!state) {
            state = {
                badge: props.badge,
                tintColor: props.tintColor
            }
        }
        return state;
    }

    componentDidMount() {
        BadgeNotificationsController.addDrawerLabelState(this.props.itemKey, this.setState.bind(this));
        this.loadBadge();
    }

    async loadBadge() {
        let badgeValue = await BadgeNotificationsController.retrieveBadgeNotificationsByRouteKey(this.props.itemKey);
        this.setState({ badge: badgeValue });
    }

    render() {
        return (
            <View style={{ flexDirection: 'row' }}>
                <Badge badgeValue={this.state.badge} />
                <Text style={[styles.labelItem, { color: this.state.tintColor }]}>{this.props.draweTitle}</Text>
            </View>
        )
    }
}

const getScreenParams = (screenStack, stackTitle, draweTitle, iconName, itemKey) => {

    let stack = createStackNavigator(screenStack, {
        defaultNavigationOptions: {
            title: stackTitle
        }
    });
    stack.navigationOptions = ({ navigation }) => {
        let drawerLockMode = 'unlocked';
        if (navigation.state.index > 0) {
            drawerLockMode = 'locked-closed';
        }

        return {
            drawerLockMode
        };
    };

    return {
        screen: stack,
        navigationOptions: {
            drawerLabel: (props) => { return <DrawerLabel tintColor={props.tintColor} itemKey={itemKey} draweTitle={draweTitle} /> },
            drawerIcon: <Icon name={iconName} color={Colors.fontColor} type='simple-line-icon' />,
        }
    }
}

export const menuItems = (application) => {

    const appTitle = application.configurations.appTitle;

    return {
        Home: getScreenParams({ Home: { screen: HomeScreen, }, PrizeScreen }, appTitle, 'Início', 'home', 'Home'),
        BuyCertification: getScreenParams(
            {
                BuyCertification: { screen: BuyCertification, },
                CheckoutScreen: { screen: CheckoutScreen, },
                PrizeScreen: { screen: PrizeScreen, },
                CheckoutWebView: { screen: CheckoutWebView }
            }, 'Escolher '+application.modality, 'Adquirir '+application.modality, 'basket', 'BuyCertification'),
        MyCertificatates: getScreenParams({ MyCertificatates: { screen: CertificatatesTransactions, }, CertificatatesTransactionsDetail }, 'Meus '+application.modality, 'Meus '+application.modality, 'basket-loaded', 'MyCertificatates'),
        MarkCertificatates: getScreenParams({ MarkCertificatates: { screen: MarkCertificatates, } }, 'Marcar '+application.modality, 'Marcar '+application.modality, 'note', 'MarkCertificatates'),
        PendingCertificatates: getScreenParams({ PendingCertificatates: { screen: CertificatatesTransactions }, CertificatatesTransactionsDetail }, application.modality+' Pendentes', application.modality+' Pendentes', 'exclamation', 'PendingCertificatates'),
        CanceledCertificatates: getScreenParams({ CanceledCertificatates: { screen: CanceledCertificatates, } }, application.modality+' Cancelados', application.modality+' Cancelados', 'close', 'CanceledCertificatates'),
        Result: getScreenParams({ Result }, 'Resultado', 'Resultado', 'list', 'Result'),
        Winners: getScreenParams({ Winners }, 'Ganhadores', 'Ganhadores', 'star', 'Winners'),
        CertificationMap: getScreenParams({ CertificationMap }, 'Onde Adquirir', 'Onde Adquirir', 'location-pin', 'CertificationMap'),
        Terms: getScreenParams({
            Terms: { screen: Terms },
            TermsDetail: { screen: TermsDetail }
        }, 'Termos', 'Termos', 'book-open', 'Terms'),
    }
}

const styles = StyleSheet.create({
    badge: {
        borderRadius: 30,
        backgroundColor: 'red',
        position: 'absolute',
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
        fontWeight: 'bold',
        color: 'white',
        top: 18,
        right: 70,
        bottom: 10,
        width: 20,
        height: 20,
    },
    labelItem: {
        flex: 1,
        fontWeight: 'bold',
        padding: 18
    }
});
