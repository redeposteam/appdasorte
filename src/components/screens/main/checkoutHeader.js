
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'
import Dimens from '../../../resources/dimens'
import Colors from '../../../resources/colors'
import Application from '../../../appConfig/application'

export default class CheckoutHeader extends Component {    

    state = {
        badge: 0
    }

    constructor() {
        super();
        this.application = Application.instance;
    }

    componentDidMount() {
        this.key = this.props.navigation.state.routeName;        
        this.application.addBadgeFunc(
            this.key,
            this.setBadge.bind(this));
        this.setBadge(this.application.checkoutBadge);
    }

    componentWillUnmount() {
        this.application.removeBadgeFunc(this.key);
    }

    setBadge(badgeNumber) {
        this.setState({ badge: badgeNumber });
    }

    render() {
        if (this.state.badge === 0) {
            return null
        } else {
            return (
                <View style={{ padding: 5 }}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        this.props.navigation.dangerouslyGetParent().navigate('CheckoutScreen');
                        // if (!this.props.action) {
                        //     this.props.navigation.navigate('BuyCertification', { checkout: true });
                        // } else {
                        //     this.props.action();
                        // }
                    }}>
                        <View style={{ margin: Dimens.halfDefaultPadding, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Icon size={30} style={{ position: 'absolute', left: 0, top: 0, right: 0, bottom: 0, }} name='basket' color='white' type='simple-line-icon' />
                            <View style={{ backgroundColor: 'white', borderRadius: 200, position: 'absolute', width: 18, height: 18, right: -5, fontSize: 11, }}>
                                <Text style={{ color: Colors.fontColor, textAlign: 'center', fontSize: 13 }}>{this.state.badge}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }
    }
}