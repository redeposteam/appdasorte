import React from 'react'
import {
    Text,
    Alert,
    View,
    SafeAreaView,
    StyleSheet,
    FlatList,
    RefreshControl,
    TouchableOpacity,
    Platform
} from 'react-native'
import Colors from '../../../resources/colors'
import Loading from '../../loading/loading'
import CertificatesController from '../../../data/certficate/certificateController'
import constants from '../../../services/constants'
import Toast from 'react-native-simple-toast'
import RowItem from './lottery/rowItem'
import BoxButton from '../../button/boxButton'
import {Icon} from 'react-native-elements'
import AppComponent from '../../appComponent'
import NoResult from '../../noResults'
import withPreventDoubleClick from '../../../util/withPreventDoubleClick'
import {NavigationEvents} from 'react-navigation'
import CountDownBar from '../../countDownBar'
import Application from "../../../appConfig/application";

const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

export default class BuyCertification extends AppComponent {

    state = {
        hideLoading: true,
        errorMsg: null,
        sellValue: '',
        refreshing: false,
        data: null,
    }

    static navigationOptions = ({navigation}) => {
        return AppComponent.getNavigationOptions(navigation, 'Escolher '+Application.instance.modality, () => {
            navigation.dangerouslyGetParent().navigate('CheckoutScreen');
        });
    }

    onWillFocus = () => {
        super.componentDidMount();
        this.getNewCertificates();
        let goToCheckout = this.props.navigation.dangerouslyGetParent().getParam('checkout', false);
        if (goToCheckout) {
            this.props.navigation.dangerouslyGetParent().setParams({checkout: false})
            this.openCheckout();
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        this.willFocusEvent.remove();
    }

    openCheckout() {
        this.props.navigation.dangerouslyGetParent().navigate('CheckoutScreen');
    }

    getNewCertificates() {

        if (!this.isEditionValid()) {
            this.setState({errorMsg: constants.APP_MSG_TIMEOUT});
            return;
        }

        let product = this.params.info.edicao_atual.produtos[0];
        this.state.sellValue = product.valor ? product.valor : product.valores;

        this.showLoading(' ');

        //this.responseSuccess(result);

        CertificatesController.getNewCertificatesFromApi(this.params, this.userData)
            .then((result) => {
                this.responseSuccess(result);
            }).catch((error) => {
            this.responseError(error);
        });
    }

    getNewCertificatesFromApi() {
        this.showLoading('Carregando mais ' + this.application.modality + ' ...');
        CertificatesController.getNewCertificatesFromApi(this.params, this.userData)
            .then((result) => {
                this.responseSuccess(result);
            }).catch((error) => {
            this.hideLoading();
            //Toast.show(constants.APP_MSG_TIMEOUT, Toast.LONG);
            this.logMyErrors(error);
        })
    }

    responseSuccess(result) {
        if (result.data.success === 'true') {
            this.shownCertificates = 2;
            let data = result.data.data;
            this.setState({data: data, refreshing: false});
        } else {
            this.setState({errorMsg: constants.APP_MSG_TIMEOUT, refreshing: false});
        }
        this.hideLoading();
    }

    responseError(error) {
        this.setState({errorMsg: constants.APP_MSG_TIMEOUT, refreshing: false});
        this.hideLoading();
        this.logMyErrors(error);
    }

    showAlert(title, msg) {
        Alert.alert(title, msg);
    }

    showLoading(message, callback) {
        this.setState({hideLoading: false, loadingText: message}, callback);
    }

    hideLoading() {
        this.setState({hideLoading: true});
    }

    saveSelectedCertificate = async (index, selectedValue) => {
        let selectedCertificate = this.state.data[index];
        return CertificatesController.addSelectedCertificate(selectedCertificate, selectedValue)
            .then((obj) => {
                this.shownCertificates--;
                selectedCertificate.selected = true;
                this.application.setCheckoutNumber(obj.count);
                this.logAddToCheckout(obj.value, obj.itemId, this.application.configurations.base);
            }).catch((error) => {
                //Toast.show('Não foi possível adicionar o certificado', Toast.LONG);
                this.logMyErrors(error);
            });
    }

    logAddToCheckout(value, itemId, base) {
        let itemName = this.application.modality;
        let itemType = itemName + ' ' + base;
        super.logAddToCart(parseFloat(value), itemType, itemName, itemId);
    }

    keyExtractor = (item, index) => index.toString();

    async onRemove(index, selectedValue) {
        let textModality = 'Adicionando ' + this.application.modality;
        this.showLoading(textModality);
        setTimeout(async () => {
            await this.saveSelectedCertificate(index, selectedValue);
            this.hideLoading();
            if (this.shownCertificates == 0) {
                this.getNewCertificatesFromApi();
            }
            Toast.show(this.application.modality+' adicionado ao carrinho.', Toast.SHORT);
        }, 500);
    }

    renderItem = ({item, index}) => {
        if (!item.selected && item.itens) {
            return (
                <RowItem
                    onRemove={this.onRemove.bind(this)}
                    item={item} index={index}
                    sequentialType={this.sequentialType}
                    index={index}
                    sellValue={this.state.sellValue}/>
            )
        }

        return null;
    }

    render() {
        let textModality = 'Mais ' + this.application.modality;
        if (this.state.errorMsg) {
            return <View style={{flex: 1}}>
                <NavigationEvents
                    onWillFocus={() => {
                        this.onWillFocus()
                    }}/>
                <NoResult hasEdition={this.hasEdition}/>
            </View>
        }

        if (this.state.data) {

            const finalizeButton = this.application.checkoutBadge > 0 ?
                <View style={{
                    position: 'absolute',
                    bottom: 65,
                    paddingHorizontal: 10,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'transparent'
                }}>
                    <NavigationEvents
                        onWillFocus={() => {
                            this.onWillFocus()
                        }}/>
                    <BoxButton
                        text='Finalizar Compra'
                        width={'85%'}
                        style={{elevation: 2}}
                        color={Colors.positiveGreen} action={() => {
                        this.props.navigation.dangerouslyGetParent().navigate('CheckoutScreen');
                    }}/>
                </View> : null

            const prizeButton = this.isTabuled ? <TouchableOpacityEx style={{flex: 0.6, flexDirection: 'row', marginRight: 10, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.dangerouslyGetParent().navigate('PrizeScreen')}>
                <NavigationEvents
                    onWillFocus={() => {
                        this.onWillFocus()
                    }}/>
                <Text style={{minWidth: 50, color: Colors.linkBlue, marginRight: 5}}>Premiações</Text>
                <Icon name='arrow-right-circle' color={Colors.linkBlue} type='simple-line-icon'/>
            </TouchableOpacityEx> : null;


            return (
                <View style={{flex: 1, backgroundColor: Colors.defaultBackground}}>
                    <NavigationEvents
                        onWillFocus={() => {
                            this.onWillFocus()
                        }}/>
                    <FlatList
                        style={{color: this.mainColor}}
                        contentContainerStyle={{paddingBottom: 50}}
                        data={this.state.data}
                        ListHeaderComponent={
                            <CountDownBar
                                params={this.params}/>
                        }
                        keyExtractor={this.keyExtractor}
                        renderItem={this.renderItem}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => {
                                    this.getNewCertificatesFromApi();
                                    this.setState({refreshing: true});
                                }}
                                title="Puxe para atualizar"
                                tintColor={this.mainColor}
                                titleColor={this.mainColor}
                                colors={[this.mainColor]}
                            />
                        }
                        refreshing={this.state.refreshing}/>
                    {finalizeButton}
                    <View style={[styles.moreCertContainer, {height: 60}]}>
                        <BoxButton style={{flex: 1, marginHorizontal: 10, alignItems: 'center'}} width={'85%'}
                                   color={Colors.blackTransparent} action={() => {
                            this.getNewCertificatesFromApi();
                        }}>
                            <Text style={{marginRight: 8, color: 'white'}}>{textModality}</Text>
                            <Icon name='reload' color={'white'} type='simple-line-icon'/>
                        </BoxButton>
                        {prizeButton}
                    </View>
                    <Loading loadColor={this.mainColor} text={this.state.loadingText} defaultText
                             hide={this.state.hideLoading}/>
                </View>
            )
        } else {
            return <View style={{flex: 1}}>
                <NavigationEvents
                    onWillFocus={() => {
                        this.onWillFocus()
                    }}/>
                <Loading hide={false}/>
            </View>
        }
    }


}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: 'white',
    },
    card: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        backgroundColor: 'white'
        //alignItems: 'center'
    },
    rowLine: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'gray',
    },
    moreCertContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 1,
        borderTopColor: Colors.defaultBackground,
        position: 'relative',
        bottom: 0,
        right: 0,
        left: 0,
        backgroundColor: '#FFFFFF',
        // backgroundColor: Colors.blackTransparent,
    }
});
