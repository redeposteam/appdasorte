import React from 'react'
import {ScrollView, StyleSheet, FlatList, Animated, View, Text, TouchableOpacity, TextInput} from 'react-native'
import Dimens from '../../../resources/dimens'
import Colors from '../../../resources/colors'
import CertificatesController from '../../../data/certficate/certificateController'
import Loading from '../../loading/loading'
import CheckoutRowItem from './lottery/checkoutRowItem'
import BoxButton from '../../button/boxButton'
import AppComponent from '../../appComponent'
import HTML from 'react-native-render-html'
import Modal from 'react-native-modalbox'
import Toast from 'react-native-simple-toast'
import Constants from '../../../services/constants'
import {Icon} from 'react-native-elements'
import LinkButton from '../../button/linkButton'

export default class CheckoutScreen extends AppComponent {

    static navigationOptions = ({navigation}) => {
        return AppComponent.checkoutScreenHeader(navigation, 'Confirmar');
    }

    state = {
        hideLoading: true,
        data: null,
        errorMsg: '',
        total: 0,
        isDisabled: true,
        promoCode: null
    }

    constructor(props) {
        super(props);

        this._animated = new Animated.Value(1);
    }

    componentDidMount() {
        super.componentDidMount();
        this.loadCertificates();
    }

    componentWillUnmount() {
    }

    loadCertificates() {
        this.showLoading();
        CertificatesController.retrieveSeletedCertificates()
            .then((result) => {
                if (result && result.certificates) {
                    let arr = Object.values(result.certificates);
                    this.setState({data: arr, total: result.total});
                }
                this.hideLoading();
            }).catch((error) => {
            this.hideLoading();
            this.logMyErrors(error);
        });
    }

    showLoading(message, callback) {
        this.setState({hideLoading: false, loadingText: message}, callback);
    }

    hideLoading() {
        this.setState({hideLoading: true});
    }

    keyExtractor = (item, index) => index.toString();

    async onRemove(item) {
        try {
            let promise = CertificatesController.deleteSelectedCerticatesData(item.sequencial);
            item.deleted = true;
            return promise;
        } catch (error) {
            alert('Não foi possível deletar o ' + this.application.modality + '.');
            this.hideLoading();
            this.logMyErrors(error);
        }
    }

    async deleteItem(item) {
        this.showLoading(' ');
        let result = await this.onRemove(item);
        setTimeout(() => {
            this.setState({total: result.total});
            this.application.setCheckoutNumber(result.count);
            this.hideLoading();
            if (result.total === 0) {
                Toast.show('O seu carrinho está vazio.', Toast.SHORT);
                this.props.navigation.goBack();
            } else {
                Toast.show('O ' + this.application.modality + ' foi removido do carrinho.', Toast.SHORT);
            }
        }, 500);
    }

    renderItem = ({item, index}) => {

        if (item.deleted) {
            return null;
        }

        if (item.itens) {
            return (
                <CheckoutRowItem
                    deleteAction={this.deleteItem.bind(this)}
                    item={item} index={index}
                    sequentialType={this.sequentialType}
                    index={index}/>
            )
        }

        return null;
    }

    finilize() {

        // this.props.navigation.replace('CheckoutWebView', {
        //     uri: "https://stackoverflow.com/questions/46451240/react-native-how-to-access-refs"
        // });
        // return

        if (this.application.userData) {
            this.showLoading();
            const userData = {
                ...this.application.userData,
                promoCode: this.state.promoCode
            }
            CertificatesController.sellInitialization(userData, this.params, this.application.__flavor, this.application.modality)
                .then((result) => {
                    if (result.data.success === 'true') {
                        if (this.params.payment === 'AIXMOBIL') {
                            this.setState({isDisabled: false, hideLoading: true}, async () => {
                                await this.application.removeAllCertificatesData();
                                let url = this.application.finalizeSellUrl + result.data.data + "&base=" + this.params.info.empresa.id_empresa;
                                console.log(url);
                                this.props.navigation.replace('CheckoutWebView', {
                                    uri: url, callback: async (res) => {
                                        if (res == 1) {
                                            this.props.navigation.dangerouslyGetParent().navigate('Home');
                                        }
                                    }
                                });
                            });
                        } else {
                            this.openUrl(this.application.finalizeSellUrl + result.data.data);
                            this.setState({
                                isDisabled: false,
                                hideLoading: true,
                                requestMessage: 'Seu pedido está sendo processado. Em breve retornaremos mais informações.'
                            });
                            this.refs.modal3.open();
                            this.application.removeAllCertificatesData();
                        }
                    } else {
                        this.setState({isDisabled: false, hideLoading: true, requestMessage: result.data.message});
                        this.refs.modal3.open();
                    }
                }).catch((error) => {
                this.hideLoading();
                alert(Constants.APP_MSG_TIMEOUT);
                this.logMyErrors(error);
            });
        } else {
            let drawerNav = this.props.navigation.dangerouslyGetParent();
            let mainNav = drawerNav.dangerouslyGetParent();
            mainNav.navigate('Profile', {
                barColor: this.mainColor, callback: () => {
                    this.finilize();
                }
            });
        }
    }

    render() {

        let modalWidth = this.dimensions.width * 0.8;
        let modalHeight = this.dimensions.height * 0.65;
        let promotionalContent = this.params.mostrar_codigo_promocional ? <TouchableOpacity style={{padding: 8}}
                                                                                            onPress={() => this.refs.modalPromo.open()}>
            <Icon name='sale' color={'red'} type='material-community'/>
        </TouchableOpacity> : null;

        return (
            <View style={{flex: 1, backgroundColor: Colors.defaultBackground}}>
                <FlatList
                    contentContainerStyle={{paddingBottom: 50}}
                    data={this.state.data}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem}/>
                <Text style={styles.errorText}>{this.state.errorMsg}</Text>
                <View style={styles.moreCertContainer}>
                    {promotionalContent}
                    <Text style={{
                        color: Colors.fontColor,
                        paddingHorizontal: 20,
                        textAlign: 'center',
                        fontWeight: 'bold',
                        fontSize: 20
                    }}>
                        Total:
                        <Text style={{color: Colors.fontColor, fontSize: 18}}>
                            {' R$ ' + Number.parseFloat(this.state.total).toFixed(2)}
                        </Text>
                    </Text>
                    <BoxButton width={90} text='Finalizar' action={() => {
                        this.finilize();
                    }}/>
                </View>
                <Modal style={[styles.modal3, {height: modalHeight, width: modalWidth}]} swipeToClose={false}
                       position={"center"} ref={"modal3"} isDisabled={this.state.isDisabled}>
                    <View style={{flex: 1}}>
                        <ScrollView contentContainerStyle={{justifyContent: 'center'}} style={{height: modalHeight,}}>
                            <Text style={{
                                textAlign: 'center',
                                fontWeight: 'bold',
                                fontSize: 15,
                                color: Colors.fontColor,
                                marginBottom: 15
                            }}>Aviso</Text>
                            <HTML html={this.state.requestMessage}/>
                        </ScrollView>
                    </View>
                    <View>
                        <BoxButton text='OK' color={Colors.linkBlue} action={() => {
                            this.refs.modal3.close();
                            this.props.navigation.goBack();
                            this.props.navigation.dangerouslyGetParent().navigate('Home');
                        }}/>
                    </View>
                </Modal>
                <Modal
                    ref={"modalPromo"}
                    backdropPressToClose={false}
                    style={{
                        width: '90%',
                        height: 170,
                        paddingVertical: 16,
                        paddingHorizontal: 16,
                        justifyContent: 'space-between'
                    }}>
                    <Text style={{fontSize: 20, color: 'black'}}>Código promocional</Text>
                    <TextInput
                        placeholder='Coloque aqui seu código promocional'
                        placeholderTextColor={Colors.divisor}
                        value={this.state.promoCode}
                        maxLength={10}
                        onChangeText={(text) => {
                            this.setState({promoCode: text})
                        }}
                        style={[styles.input]}/>
                    <View style={{
                        alignSelf: 'flex-end',
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                        width: 165,
                    }}>
                        <LinkButton text={'Cancelar'}
                                    color={Colors.fontColor}
                                    action={() => {
                                        this.refs.modalPromo.close();
                                        this.setState({promoCode: null});
                                    }}/>
                        <LinkButton text={'Adicionar'}
                                    action={() => this.refs.modalPromo.close()}/>
                    </View>
                </Modal>
                <Loading loadColor={this.mainColor} text={this.state.loadingText} defaultText
                         hide={this.state.hideLoading}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    errorText: {
        textAlign: 'center',
        padding: Dimens.defaultPadding
    },
    moreCertContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        alignItems: 'center',
        borderTopWidth: 1,
        borderTopColor: Colors.defaultBackground,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        backgroundColor: '#FFFFFFF2',
        // backgroundColor: Colors.blackTransparent,
        height: 60,
    },

    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    modal3: {
        height: 300,
        width: 300,
        padding: 20
    },
    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    input: {
        width: '100%',
        height: 50,
        backgroundColor: "#f5f7f9",
        borderRadius: 10,
        color: Colors.fontColor,
        borderWidth: 1,
        alignItems: 'center',
        padding: 8,
        borderColor: '#e7e9ed',
        flexDirection: 'row'
    },
});
