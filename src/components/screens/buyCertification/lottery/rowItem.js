
import React, { Component } from 'react'
import { TouchableOpacity, Text, View, Animated, Picker, StyleSheet } from 'react-native'
import Colors from '../../../../resources/colors'
import Dimens from '../../../../resources/dimens'
import { Icon } from 'react-native-elements'
import BoxButton from '../../../button/boxButton'
import DivisorElement from '../../../lottery/divisorElement'
import Lottery, { LineType } from './lottery'

const ANIMATION_DURATION = 300;
const initialVal = 'Valor da compra';

export default class RowItem extends Component {

    state = {
        selectedValue: initialVal
    }

    constructor(props) {
        super(props);
        this._animated = new Animated.Value(1);
    }

    getContent = (showDivisor, index) => {
        if (showDivisor) {
            return <DivisorElement title={'Combinação ' + (index + 1)} />
        }
        return null;
    }

    cardTitle(obj) {
        if (obj && obj.sequencial) {
            let strNumber = obj.sequencial.substring(0, 3) + '.' + obj.sequencial.substring(2, 6);
            return 'Número do ' + Application.instance.modality + ': ' + strNumber
        }
        return null;
    }

    async action(selectedValue) {
        if (selectedValue === initialVal) {
            alert('Selecione um valor para a adquirir.');
            return;
        }
        this.props.onRemove(this.props.index, selectedValue);
    }

    getFormatedSellValue(value) {
        return 'R$ ' + parseFloat(value).toFixed(2);
    }

    createButtonComponent() {

        if (this.props.sellValue instanceof Array) {
            return (
                <View style={{ padding: 10, }}>
                    <View style={{ width: '100%', flexDirection: 'row', padding: 10 }}>
                        <Picker
                            selectedValue={this.state.selectedValue}
                            style={{ flex: 1, height: 50, marginRight: 10 }}
                            onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue: itemValue })}>
                            <Picker.Item label={initialVal} value='' />
                            {
                                this.props.sellValue.map((val, index) => {
                                    return <Picker.Item key={index} label={this.getFormatedSellValue(val)} value={val} />
                                })
                            }
                        </Picker>
                        <BoxButton width={100} color={Colors.linkBlue} action={() => {
                            this.action(this.state.selectedValue)
                        }}>
                            <Icon name='add-shopping-cart' color='white' type='MaterialIcons' />
                        </BoxButton>
                    </View>
                </View>
            )
        } else {
            return <View style={{ width: '100%', flexDirection: 'row', padding: 10 }}><BoxButton color={Colors.linkBlue} action={() => {
                { this.action(this.props.sellValue) }
            }}>
                <Text style={{ paddingHorizontal: Dimens.defaultPadding, fontWeight: 'bold', color: 'white', }}>{this.getFormatedSellValue(this.props.sellValue)}</Text>
                <Icon name='add-shopping-cart' color='white' type='MaterialIcons' />
            </BoxButton>
            </View>
        }
    }
    
    render() {
        return (
            <Lottery                
                lineType={LineType.GRID}
                item={this.props.item}
                sequentialType={this.props.sequentialType}         
                deleteAction={this.props.deleteAction}>
                {this.createButtonComponent()}                
            </Lottery>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        padding: Dimens.defaultPadding,
        backgroundColor: Colors.positiveGreen,
        borderRadius: 5,
        width: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    textButton: {
        color: 'white',
        alignSelf: 'center',
        fontWeight: 'bold'
    }
});
