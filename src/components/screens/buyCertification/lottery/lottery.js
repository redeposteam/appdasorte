import React, {Component} from 'react'
import {Text, View, StyleSheet, Dimensions, TouchableOpacity} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import {SequentialType} from "../../../appComponent";
import Colors from '../../../../resources/colors'
import Card from '../../../lottery/card'
import Dimens from '../../../../resources/dimens'
import {Icon} from 'react-native-elements'
import Application from "../../../../appConfig/application";

export const LineType = {
    IN_LINE: 1,
    TWO_LINES: 2,
    GRID: 3
}

var {height, width} = Dimensions.get('window');

export class TitleWithDivisor extends Component {

    render() {

        if (!this.props.title || this.props.title.trim().length === 0) {
            return null;
        }

        let deleteIcon = this.props.deleteAction ? <Icon name='delete' color='red' type='MaterialIcons' onPress={() => {
            if (this.props.item) {
                this.props.deleteAction(this.props.item);
            }
        }}/> : null;

        let textAlign = 'center';
        if (this.props.deleteAction || this.props.textAlignLeft) {
            textAlign = 'left';
        }

        return (
            <View style={this.props.style}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                    <Text style={[styles.divisorTitle, {textAlign: textAlign}]}>{this.props.title}</Text>
                    {deleteIcon}
                </View>
                <View style={styles.divisor}/>
            </View>
        );
    }
}

export class DozensContainer extends Component {

    state = {
        dozensElements: []
    }

    componentDidMount() {
        //this.createDozensElements();
    }

    createDozensElements() {
        if (this.props.dozens && this.props.dozens.length > 0) {
            switch (this.props.lineType) {
                case LineType.IN_LINE:
                    return this.createLineDozensElements(this.props.dozens);
                case LineType.TWO_LINES:
                    return this.createTwoLinesDozensElements(this.props.dozens);
                case LineType.GRID:
                    return this.createGridDozensElements(this.props.dozens);

                default:
                    return this.createLineDozensElements(this.props.dozens);
            }
        }
        return null;
    }

    createGridDozensElements(dozens) {

        let dozensEls = [];
        let dozensLines = [];
        this.dozensElements = [];

        dozens.forEach((dozen, index) => {
            dozensEls.push(<Dozen
                key={index}
                dozen={dozen}
                idEdition={this.props.idEdition}
                certificateSequential={this.props.certificateSequential}
                lineType={this.props.lineType}
                onDozenTap={this.props.onDozenTap}
                onCreatingDozen={this.props.onCreatingDozen}
                onUmountDozen={this.props.onUmountDozen}/>);
            if ((index + 1) % 5 === 0) {
                dozensLines.push(<DozenLine key={index} lineType={this.props.lineType}>{dozensEls}</DozenLine>);
                dozensEls = [];
            }
        });
        return dozensLines;
    }

    createTwoLinesDozensElements(dozens) {
        let dozensEls = [];
        let dozensLines = [];
        this.dozensElements = [];

        let halfIndex = Math.floor((dozens.length) / 2);
        let firstHalf = dozens.slice(0, halfIndex);
        let secondHalf = dozens.slice(halfIndex, dozens.length);

        firstHalf.forEach((dozen, index) => {
            dozensEls.push(<Dozen key={index} dozen={dozen} lineType={this.props.lineType}/>);
        });
        dozensLines.push(<DozenLine key={1} lineType={this.props.lineType}>{dozensEls}</DozenLine>);
        dozensEls = [];
        secondHalf.forEach((dozen, index) => {
            dozensEls.push(<Dozen key={index} dozen={dozen} lineType={this.props.lineType}/>);
        });
        dozensLines.push(<DozenLine key={2} lineType={this.props.lineType}>{dozensEls}</DozenLine>);

        return dozensLines;
    }

    createLineDozensElements(dozens) {
        let dozensEls = [];
        dozens.forEach((dozen, index) => {
            dozensEls.push(<Dozen lineType={this.props.lineType} key={index} dozen={dozen}/>);
        });
        return <View style={{flexDirection: 'row', flexWrap: 'wrap', padding: 10}}>{dozensEls}</View>;
        //this.setState({ dozensElements: <View style={{ flexDirection: 'row', flexWrap: 'wrap', padding: 10 }}>{dozensEls}</View> });
    }

    render() {
        return (
            <View style={styles.dozenContainer}>
                {this.createDozensElements()}
            </View>
        )
    }
}

class DozenLine extends Component {

    render() {
        return (
            <View style={styles.dozenLine}>
                {this.props.children}
            </View>
        )
    }
}

class Dozen extends Component {

    state = {
        marked: false
    }

    componentDidUpdate(prevProps) {
        //alert(prevProps.dozen)
        if (prevProps.onCreatingDozen) {
            prevProps.onCreatingDozen(this.dozenTap.bind(this), prevProps.dozen, prevProps.certificateSequential, prevProps.idEdition);
        }
    }

    componentDidMount() {
        if (this.props.onCreatingDozen) {
            this.props.onCreatingDozen(this.dozenTap.bind(this), this.props.dozen, this.props.certificateSequential, this.props.idEdition);
        }
    }

    componentWillUnmount() {
        if (this.props.onUmountDozen) {
            this.props.onUmountDozen(this.props.dozen, this.props.certificateSequential, this.props.idEdition);
        }
    }

    getSizeElement(lineType) {
        switch (lineType) {
            case LineType.IN_LINE:
                return {size: 35, fontSize: 20};
            case LineType.TWO_LINES:
                let size = (width - 90) / 10;
                return {size: size, fontSize: size * 0.55};
            case LineType.GRID:
                return {size: 53, fontSize: 28};

            default:
                return {size: 35, fontSize: 20};
        }
    }

    async dozenTap(markedValue) {
        return new Promise((resolve, reject) => {
            if (markedValue == undefined) {
                markedValue = !this.state.marked;
            }
            this.setState({marked: markedValue}, () => resolve(this.props.dozen));
        });
    }

    render() {

        let sizeEl = this.getSizeElement(this.props.lineType);
        let size = sizeEl.size;
        let fontSize = sizeEl.fontSize;
        let markedStyle = this.state.marked ? styles.markedStyle : {};

        let content = (
            <LinearGradient
                style={[styles.gradientDozenContainer, {height: size, width: size}]}
                colors={['#F5F5F5', 'white', Colors.defaultBackground]}>
                <Text style={[styles.dozenText, {fontSize: fontSize}]}>{this.props.dozen}</Text>
            </LinearGradient>
        )

        if (this.props.onDozenTap) {
            return (
                <TouchableOpacity
                    onPress={() => {
                        this.props.onDozenTap(this.props.dozen, this.state.marked, this.props.certificateSequential);
                        //this.setState({ marked: !this.state.marked });
                    }}>
                    {content}
                    <View style={markedStyle}/>
                </TouchableOpacity>
            )
        }

        return content;
    }
}

export default class Lottery extends Component {

    cardTitle(obj) {
        if (obj && obj.sequencial) {
            let strNumber = obj.sequencial.toString();

            let luckNumber = '';
            if (this.props.sequentialType !== SequentialType.None) {
                luckNumber = 'Nº da sorte: ' + obj.sequencial;
                if (this.props.sequentialType === SequentialType.Child) {
                    luckNumber = '';
                    obj.itens.forEach((item, index) => {
                        let lineEnd = '    ';
                        luckNumber = luckNumber + 'Nº da sorte ' + (index + 1) + ': ' + item.sequencial + lineEnd;
                    });
                }
            }

            if (this.props.showValue) {
                let value = obj.value ? obj.value : obj.valor;
                return 'Número do ' + Application.instance.modality + ': ' + strNumber + '\n' + luckNumber + '\nValor: R$ ' + ((parseFloat(value)).toFixed(2));
            } else {
                return 'Número do ' + Application.instance.modality + ': ' + strNumber + '\n' + luckNumber;
            }
        }
        return null;
    }

    getCombination() {
        // if (this.combination) {
        //     return this.combination;
        // }
        let getCombinationText = this.props.item.itens.length > 1 ?
            (index) => {
                return 'Combinação ' + (index + 1)
            } : () => {
                return ''
            };

        return this.combination = this.props.item.itens.map((item, index) => {
            return (
                <View key={index} style={{paddingHorizontal: 10}}>
                    <TitleWithDivisor title={getCombinationText(index)}/>
                    <DozensContainer
                        lineType={this.props.lineType}
                        dozens={item.dezenas.split('-')}
                        certificateSequential={item.sequencial}
                        onDozenTap={this.props.onDozenTap}
                        idEdition={item.idEdition}
                        onCreatingDozen={this.props.onCreatingDozen}
                        onUmountDozen={this.props.onUmountDozen}/>
                </View>
            )
        })
    }

    render() {

        return (
            <LinearGradient
                colors={['white', 'white', Colors.defaultBackground]}
                style={styles.gradient}>
                <TitleWithDivisor
                    title={this.cardTitle(this.props.item)}
                    item={this.props.item}
                    deleteAction={this.props.deleteAction}
                    textAlignLeft={this.props.textAlignLeft}/>
                <Card>
                    {this.getCombination()}
                    {this.props.children}
                </Card>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    gradient: {
        padding: 10
    },
    titleContainer: {},
    title: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingBottom: 10,
        fontWeight: 'bold',
        flex: 1
    },
    divisor: {
        width: '100%',
        height: 1,
        marginTop: 10,
        backgroundColor: Colors.defaultBackground,
        marginBottom: Dimens.defaultPadding
    },
    divisorTitle: {
        fontWeight: 'bold',
        flex: 1
    },
    gradientDozenContainer: {
        height: 45,
        width: 45,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
        borderRadius: 10,
        borderWidth: 0,
        borderColor: 'gray',
        marginHorizontal: 3
    },
    dozenContainer: {
        paddingVertical: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    dozenLine: {
        flexDirection: 'row',
        padding: 3,
    },
    dozenText: {
        fontSize: 25,
        fontWeight: 'bold'
    },
    markedStyle: {
        backgroundColor: '#007AFF63',
        position: 'absolute',
        top: 0,
        margin: 2.5,
        bottom: 0,
        left: 0,
        right: 0,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: 'gray',
    },
});
