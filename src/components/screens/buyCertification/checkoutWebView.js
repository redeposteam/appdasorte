import React from 'react';
import AppComponent from '../../appComponent';
import {View, Alert, BackHandler, TouchableOpacity} from 'react-native';
import {WebView} from 'react-native-webview';
import {NavigationEvents} from 'react-navigation';
import Toast from 'react-native-simple-toast';
import Application from "../../../appConfig/application";

export default class CheckoutWebView extends AppComponent {


    static navigationOptions = ({navigation}) => {
        return AppComponent.checkoutScreenHeader(navigation, 'Finalizar');
        // , <TouchableOpacity style={{ marginStart: 16 }} onPress={() => {
        //     CheckoutWebView.backFunction(navigation);
        // }}>
        //     <Icon name='arrow-left' color={'white'} type='material-community' />
        // </TouchableOpacity>);
    }

    static backFunction = () => {
    }

    constructor(props) {
        super(props);
        const uri = props.navigation.getParam('uri', '');
        this.callback = props.navigation.getParam('callback', null);
        //BackHandler.addEventListener("hardwareBackPress", this.onBack);

        this.state = {
            uri: uri
        }
    }

    componentDidMount() {
        // CheckoutWebView.backFunction = (navigation) => {
        //     if (!this.webView.canGoBack) {
        //         navigation.goBack();
        //     } else {
        //         this.webView.goBack();
        //     }
        // }
    }

    componentWillUnmount() {
        //BackHandler.removeEventListener("hardwareBackPress", this.onBack);
    }

    onWillBlur = () => {
        if (!!this.goBackFunc) {
            clearTimeout(this.goBackFunc);
        }
        if (!!this.eventData) {
            this.callback(this.eventData);
        }
    }

    // onBack = () => {
    //     CheckoutWebView.backFunction(this.props.navigation);
    //     return true;
    // }

    render() {
        return (
            <View style={{flex: 1}}>
                <NavigationEvents
                    onWillBlur={() => {
                        this.onWillBlur()
                    }}/>
                <WebView
                    ref={(webView) => {
                        this.webView = webView;
                    }}
                    source={{uri: this.state.uri}}
                    startInLoadingState
                    javaScriptEnabledAndroid={true}
                    javaScriptEnabled={true}
                    // onNavigationStateChange={(navState) => {
                    //     alert(navState.canGoBack)
                    //     this.webView.canGoBack = navState.canGoBack;
                    // }}
                    onMessage={event => {
                        if (event.nativeEvent.data == 1) {
                            this.eventData = event.nativeEvent.data;
                            Toast.show("Sua compra foi realizada com sucesso!", Toast.SHORT);
                            this.goBackFunc = setTimeout(() => {
                                this.props.navigation.goBack();
                            }, 500);
                            // Alert.alert(
                            //     "Sucesso!",
                            //     "Sua compra foi realizada com sucesso!",
                            //     [
                            //         {
                            //             text: "OK", onPress: () => {
                            //                 this.props.navigation.goBack();
                            //             }
                            //         }
                            //     ],
                            //     { cancelable: false }
                            // );
                        }
                    }}
                />

            </View>
        )
    }
}