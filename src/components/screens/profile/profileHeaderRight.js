
import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import Dimens from '../../../resources/dimens';

export default navigationOptions = (navigation, title) => {
    return {
        title: title,
        headerRight:
            <TouchableOpacity style={{ padding: Dimens.defaultPadding }} onPress={() => { navigation.goBack() }}>
                <Text style={styles.textButton} >Fechar</Text>
            </TouchableOpacity>,
        headerLeft: null
    }
}

const styles = StyleSheet.create({
    textButton: {
        color: 'white',
        alignSelf: 'center'
    },
});