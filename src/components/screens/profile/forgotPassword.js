
import React from 'react'
import { View, TextInput, StyleSheet, Keyboard } from 'react-native'
import Colors from '../../../resources/colors'
import Dimens from '../../../resources/dimens'
import AppComponent from '../../appComponent'
import BoxButton from '../../button/boxButton'
import Validations from '../../../util/validations'
import UserController from '../../../data/user/userController';
import Loading from '../../loading/loading';
import Constants from '../../../services/constants'

export default class ForgotPassword extends AppComponent {

    static navigationOptions = ({ navigation }) => {
        return AppComponent.onTopNavigationOptions(navigation.getParam('barColor', 'red'), 'Esqueceu a Senha?', navigation);
    }

    validate() {
        if (!Validations.validateEmail(this.state.email)) {
            this.hideLoading();    
            this.showAlert('Erro', 'E-mail inválido.');            
            return false;
        }        
        return true;
    }

    async requestPassword() {
        Keyboard.dismiss();
        this.showLoading();
        if (this.validate()) {
            UserController.forgotPassword(this.state.email, this.params.info.empresa.id_empresa)
                .then((result) => {                    
                    if (result.data.success === 'true') {
                        this.hideLoading();
                        alert(result.data.message);
                        this.props.navigation.dangerouslyGetParent().navigate('Home');                        
                    } else {
                        this.hideLoading();
                        alert(result.data.message);
                    }
                }).catch((error) => {                    
                    this.hideLoading();
                    this.logMyErrors(error);
                    alert(Constants.APP_MSG_ERROR);
                });
        }
    }

    render() {
        return (
            <View style={styles.main}>
                <TextInput
                    placeholder='Digiste seu e-mail cadastrado'
                    {...inputAttrs}
                    value={this.state.email}
                    onChangeText={(text) => this.setState({ email: text })}
                    style={styles.input} />
                <BoxButton text='CONTINUAR' width='95%' action={() => { this.requestPassword() }} />
                <Loading loadColor={this.mainColor} hide={this.state.hideLoading} />
            </View>
        );
    }
}

const defaultWitdh = '90%';
const styles = StyleSheet.create({
    main: {
        flex: 1,
        padding: Dimens.defaultPadding,
        alignItems: 'center',
        backgroundColor: Colors.defaultBackground
    },
    input: {
        padding: Dimens.defaultPadding + Dimens.halfDefaultPadding,
        width: defaultWitdh,
        backgroundColor: 'white',
        color: Colors.fontColor,
        borderBottomColor: Colors.defaultBackground,
        borderBottomWidth: 1,
        borderRadius: 5,
        marginVertical: 10
    },
    sendButton: {
        marginTop: Dimens.defaultPadding,
        padding: Dimens.defaultPadding,
        backgroundColor: Colors.positiveGreen,
        borderRadius: Dimens.defaultRadius,
        width: '95%',
    },
    textButton: {
        color: 'white',
        alignSelf: 'center'
    },
});
const inputAttrs = { placeholderTextColor: Colors.divisor };