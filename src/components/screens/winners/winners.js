import React from 'react';
import {Image, StyleSheet, Platform, Text, FlatList, View} from 'react-native';
import AppComponent from '../../appComponent';
import Loading from '../../loading/loading';
import NoResult from '../../noResults';
import Constants from '../../../services/constants';
import ResultController from '../../../data/result/resultController';
import Colors from '../../../resources/colors';
import FitImage from 'react-native-fit-image';
import LinearGradient from 'react-native-linear-gradient';
import Card from '../../lottery/card';
import RNPickerSelect from 'react-native-picker-select';
import {Icon} from 'react-native-elements';

export default class Winners extends AppComponent {
  
  state = {
    data: [],
    errorMsg: '',
    hideLoading: false,
    selectedDate: null,
    items: [{label: '', value: ''}],
  }
  
  componentDidMount() {
    super.componentDidMount();
    this.init();
  }
  
  async init() {
    await this.loadResultsDates();
    await this.getWinnersList();
  }
  
  async loadResultsDates() {
    this.showLoading();
    return ResultController.getResultDates(this.usertoken, this.params.info.empresa.id_tipo_loteria).then((result) => {
      let dates = result.data.data;
      let items = this.createPickerItems(dates);
      this.setState({
        items: items,
        selectedDate: dates[0],
      });
    }).catch((error) => {
      this.logMyErrors(error);
    });
  }
  
  async getWinnersList(itemValue) {
    
    const selectedDate = itemValue ? itemValue : this.state.selectedDate;
    const base = this.application.configurations.base;
    let errorMsg = '';
    
    if (!selectedDate) {
      this.hideLoading(true, Constants.APP_MSG_NO_RESULT);
      return;
    }
    
    return ResultController.getWinnersList(base, selectedDate)
      .then((result) => {
        if (result.data.success === 'true') {
          const data = result.data.data;
          this.setState({
            data: data,
            hideLoading: true
          });
        } else {
          this.setState({
            data: [],
            errorMsg: Constants.APP_MSG_NO_RESULT,
            hideLoading: true
          });
        }
      }).catch((error) => {
        this.logMyErrors(error);
        this.setState({
          data: [],
          errorMsg: Constants.APP_MSG_NO_RESULT,
          hideLoading: true
        });
      });
  }
  
  createPickerItems(dates) {
    if (dates && dates.length > 0) {
      return dates.map((date, index) => {
        return (
          {label: 'Sorteio - ' + date, value: date}
        )
      });
    }
    return [];
  }
  
  getPicker() {
    
    if (this.state.data.length > 0 && Platform.OS === 'ios') {
      return (
        <RNPickerSelect
          placeholder={{}}
          doneText='OK'
          items={this.state.items}
          onValueChange={(itemValue, index) => {
            this.setState({
              selectedDate: itemValue
            });
          }}
          onOpen={() => {
            this.oldSelectedDate = this.state.selectedDate;
          }}
          onClose={() => {
            if (this.oldSelectedDate !== this.state.selectedDate) {
              this.setState({
                hideLoading: false, selectedDate: this.state.selectedDate
              }, () => this.getWinnersList(this.state.selectedDate));
            }
          }}
          style={pickerSelectStyles}
          value={this.state.selectedDate}
          Icon={() => <View style={{height: 45, justifyContent: 'center', alignItems: 'center'}}><Icon name='menu-down'
                                                                                                       color={'white'}
                                                                                                       type='material-community'/></View>}
        />
      );
    } else if (this.state.data.length > 0) {
      return (
        <RNPickerSelect
          placeholder={{}}
          items={this.state.items}
          onValueChange={(itemValue) => {
            this.setState({lotteryDate: itemValue, hideLoading: false});
            this.getWinnersList(itemValue);
          }}
          style={pickerSelectStyles}
          value={this.state.items[this.state.lotteryDate]}
          Icon={() => <View style={{height: 45, justifyContent: 'center', alignItems: 'center'}}><Icon name='menu-down'
                                                                                                       color={'white'}
                                                                                                       type='material-community'/></View>}
        />
      );
    }
  }
  
  hideLoading(hide, errorMsg) {
    this.setState({hideLoading: hide, errorMsg, errorMsg});
  }
  
  keyExtractor = (item, index) => index.toString();
  
  renderItem = ({item, index}) => {
    return (
      <LinearGradient colors={['white', 'white', Colors.defaultBackground]} style={{paddingHorizontal: 10}}>
        <Card>
          <View style={{overflow: 'hidden', width: '100%', height: 150}}>
            <FitImage
              key={item.foto}
              style={item.foto ? {} : {width: '100%', height: 100}}
              indicatorColor={this.mainColor}
              indicatorSize="large"
              source={item.foto ? {uri: item.foto} : require('../../../resources/img/noimage.jpg')}/>
          </View>
          <View style={{padding: 10}}>
            <Text style={{fontSize: 20, fontWeight: 'bold', paddingBottom: 5}}>{item.nome}</Text>
            <Text style={{fontSize: 13, fontWeight: 'bold'}}>{item.premio}</Text>
            <Text style={{fontSize: 13, fontWeight: 'bold'}}>LOCALIDADE: {item.endereco}</Text>
          </View>
        </Card>
      </LinearGradient>
    );
  };
  
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <FlatList
          data={this.state.data}
          ListEmptyComponent={<NoResult errorMsg={this.state.errorMsg}/>}
          ListHeaderComponent={
            <View style={{
              borderBottomColor: Colors.divisor,
              borderBottomWidth: 1,
              paddingHorizontal: 10,
              backgroundColor: this.mainColor + 'e6'
            }}>
              {this.getPicker()}
            </View>
          }
          renderItem={this.renderItem.bind(this)}
          keyExtractor={this.keyExtractor}/>
        <Loading
          defaultText
          hide={this.state.hideLoading}
          loadColor={this.mainColor}/>
      </View>
    )
    
  }
  
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 12,
    paddingHorizontal: 10,
    width: '100%',
    color: 'white',
    paddingRight: 30 // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderRadius: 8,
    color: 'white',
    paddingRight: 30 // to ensure the text is never behind the icon
  }
});