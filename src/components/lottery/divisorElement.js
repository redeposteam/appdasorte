
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Colors from '../../resources/colors'
import Dimens from '../../resources/dimens'
import { Icon } from 'react-native-elements'

export default class DivisorElement extends Component {

    render() {

        let deleteIcon = this.props.deleteButton ? <Icon name='delete' color='red' type='MaterialIcons' onPress={() => { if (this.props.deleteAction) {            
            this.props.deleteAction();
        } }} /> : null;

        let textAlign = 'center';
        if (this.props.deleteButton || this.props.textAlignLeft) {
            textAlign = 'left';
        }

        return (
            <View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ textAlign: textAlign ,fontWeight: 'bold', flex: 1 }}>{this.props.title}</Text>
                    {deleteIcon}
                </View>
                <View style={{ width: '100%', height: 1, marginTop: 10, backgroundColor: Colors.defaultBackground, marginBottom: Dimens.defaultPadding }} />
            </View>
        );
    }
}