
import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import DozenElement from './dozenElement'

const MAX_ROW = 5;

export default class LotteryCard extends Component {

    constructor() {
        super();        
    }    

    createCardNumbers() {

        let props = this.props;
        let dozens = props.dozens;

        if (props.maxRow) {
            let arrRow = new Array();
            let arrDozens = new Array();
            let maxRow = props.maxRow ? props.maxRow : MAX_ROW;

            dozens.map((d, i) => {

                let key = this.props.identifier + '_' + i;
                
                let markedVal = props.marked && props.marked[d] ? props.marked[d] : null;
                arrDozens.push(<DozenElement key={key} certificateNumber={this.props.certificateNumber} markedState={this.props.markedState} marked={markedVal} onDozenClick={props.onDozenClick} size={props.dozensSize} dozen={d} />);
                if (arrDozens.length === maxRow) {
                    arrRow.push(<View key={key} style={[styles.rowLine, { justifyContent: 'center' }]}>{arrDozens}</View>);                    
                    arrDozens = new Array();
                }
            });
            return arrRow;
        } else {
            return props.dozens.map((d, i) => {
                let key = this.props.identifier + '_' + i;
                return (
                    <DozenElement key={key} onDozenClick={props.onDozenClick} size={props.dozensSize} dozen={d} />
                )
            })
        }
    }

    render() {
        if (!this.props.dozens || this.props.dozens.length === 0) {
            return null;
        }
        if (this.props.maxRow) {
            return (
                <View style={{}}>
                    {this.props.children}
                    {this.createCardNumbers()}
                    {/* {this.state.dozensEl} */}
                </View>
            )
        } else {
            return (
                <View style={{}}>
                    {this.props.children}
                    <View style={[styles.rowLine, { justifyContent: 'flex-start' }]}>
                    {this.createCardNumbers()}
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    rowLine: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
    },
});