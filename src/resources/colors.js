
const defaultBackground = '#E0E0E0';
const blackTransparent = '#00000080';
const fontColor = '#616161';
const itemClick = '#E0E0E080';
const divisor = '#BDBDBD';
const positiveGreen = '#33CD5F';
const linkBlue = '#007AFF';

export default class Colors {

    static get defaultBackground() {
        return defaultBackground;
    }

    static get blackTransparent() {
        return blackTransparent;
    }

    static get fontColor() {
        return fontColor;
    }

    static get divisor() {
        return divisor;
    }

    static get itemClick() {
        return itemClick;
    }

    static get positiveGreen() {
        return positiveGreen;
    }

    static get linkBlue() {
        return linkBlue;
    }
}